import NativePackagerKeys._

name := """ploit"""

version := "1.0"

scalaVersion := "2.10.4"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)

pipelineStages := Seq(rjs)

resolvers ++= Seq(
  "tpolecat" at "http://dl.bintray.com/tpolecat/maven",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

libraryDependencies ++= Seq(
  "com.twitter" % "finagle-http_2.10" % "6.18.0",
  "postgresql" % "postgresql" % "9.0-801.jdbc4",
  "joda-time" % "joda-time" % "2.4",
  "org.tpolecat" %% "doobie-core" % "0.1",
  "com.github.nscala-time" %% "nscala-time" % "1.4.0",
  "jp.t2v" %% "play2-auth"      % "0.12.0",
  "jp.t2v" %% "play2-auth-test" % "0.12.0" % "test",
  "com.github.t3hnar" %% "scala-bcrypt" % "2.4"
)
