function onEverySecond(func) {
  "use strict";
  var oneSecond = 1000;
      
  setInterval(function() {
    func();
  }, oneSecond);
}
