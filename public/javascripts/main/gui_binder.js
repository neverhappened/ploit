var GUIBinder = function(timesBus, eventsBus, storage, palette, ribbon, calendar,
                      colorpickers, analogClock, statsBars, modalControl, eventList, goalList, chart, otherState) {
  "use strict";
  var that = this;

  this.bind = function() {
    adjustTimeRibbonOnResize(ribbon);
    bindAllButtons();
    bindModalHandlers();
    bindPreviousNextDateSwitchOnLeftRightKey();
  };

  this.onFetchEventsSuccess = function(newEvents) {
    var convertedEvents = PeriodUtils.convertAllFromServerFormat(newEvents);
    eventsBus.addEvents(convertedEvents);
  };

  function bindAllButtons() {
    colorpickers.create();
    bindDropDbButton();
    bindSwitchMonthsButtons();
    bindSwitchAnalogClockButton();
    bindSetDefaultsButton();
    bindPlayPeriodButton();
    bindDeleteSwitchedDateEventsButton();
    bindColorChangeOnLeftRightButtons();
    bindHideTimeribbonButton();
  }

  function bindHideTimeribbonButton() {

    var $button = $('#hide-timeribbon-button'),
        $glyph = $button.find('.glyphicon');

    $button.on('click', function() {
      var newState = !storage.loadIsTimeribbonShown();
      storage.saveIsTimeribbonShown(newState);
      switchTimeribbonOnClick(newState, $glyph);
    });

    switchTimeribbonOnClick(otherState.isTimeribbonShown, $glyph);
  }

  function switchTimeribbonOnClick(newState, $glyph) {
    if (newState) {
      ribbon.hide();
      eventsBus.removeListener(ribbon);
      timesBus.removeListener(ribbon);
      $glyph.removeClass('glyphicon-chevron-left');
      $glyph.addClass('glyphicon-chevron-right');
    }
    else {
      ribbon.show();
      eventsBus.addListener(ribbon);
      timesBus.addListener(ribbon);
      $glyph.removeClass('glyphicon-chevron-right');
      $glyph.addClass('glyphicon-chevron-left');
    }
  }

  function bindSwitchAnalogClockButton() {

    var $analog_clock_switch = $('#analog-clock-switch-checkbox');

    $analog_clock_switch.on('click', function() {
      if ($(this).prop('checked')) {
        storage.saveIsAnalogClockShown(true);
        analogClock.show();
      }
      else {
        storage.saveIsAnalogClockShown(false);
        analogClock.hide();
      }
    });

    $analog_clock_switch.prop('checked', otherState.isAnalogClockShown);
    if (otherState.isAnalogClockShown) {
      analogClock.show();
    }
    else {
      analogClock.hide();
    }
  }

  function bindSwitchMonthsButtons() {
    var switchedDate = moment(timesBus.getSwitchedDate()),
        $leftSwitch = $('#month-switch-left'),
        $rightSwitch = $('#month-switch-right');
    
    var onSwitchMonthButtonClick = function(newSwitchedDate) {
      var month = newSwitchedDate.month();

      if (month === 0) {
        $leftSwitch.attr('disabled', 'disabled');
        $leftSwitch.find('span').css({ 'color': '#aaa' });
      }
      else if (month === 11) {
        $rightSwitch.attr('disabled', 'disabled');
        $rightSwitch.find('span').css({ 'color': '#aaa' });
      }
      else {
        $leftSwitch.removeAttr('disabled');
        $rightSwitch.removeAttr('disabled');
        $leftSwitch.find('span').css({ 'color': '#777' });
        $rightSwitch.find('span').css({ 'color': '#777' });
      }

      timesBus.switchDate(newSwitchedDate);
    };

    $leftSwitch.on('click', function() {
      if (switchedDate.month() === 0) {
      }
      else {
        switchedDate.subtract(1, 'months').startOf('month');
        timesBus.switchDate(switchedDate);
      }

      onSwitchMonthButtonClick(switchedDate);
    });

    $rightSwitch.on('click', function() {
      if (switchedDate.month() === 11) {
      }
      else {
        switchedDate.add(1, 'months');
        timesBus.switchDate(switchedDate);
      }

      onSwitchMonthButtonClick(switchedDate);
    });

    onSwitchMonthButtonClick(switchedDate);
  }

  function bindDropDbButton() {
    $('#drop-db-button').on('click', function() {
      AjaxUtils.dropDb();
    });
  }

  function bindSetDefaultsButton() {
    $('#set-defaults-button').on('click', function() {
      palette.setDefaultColors();
      setDefaultColorsOnDrawables();
    });
  }

  function setDefaultColorsOnDrawables(drawables) {
    var defaultColors = palette.getColors();

    switchColors(defaultColors);
  }


  function bindPlayPeriodButton() {
    var $switchButton = $('#switch-current-time-button'),
        $glyph = $switchButton.find('.glyphicon');

    $switchButton.on('click', function() {
      if (eventsBus.isInPlayMode()) {
        var continuePeriod =  eventsBus.getCurrentContinuePeriod();
        $glyph.removeClass('glyphicon-stop');
        $glyph.addClass('glyphicon-play');

        eventsBus.stopContinuePeriod();

        if (continuePeriod.getFrom().isSame(timesBus.getNowDate(), 'minute')) {
          eventsBus.endContinuePeriod();
        }
      }
      else {
        $glyph.removeClass('glyphicon-play');
        $glyph.addClass('glyphicon-stop');
        eventsBus.playContinuePeriod();
        timesBus.switchDate(moment(eventsBus.getCurrentContinuePeriod().getFrom()).startOf('day'));
      }
    });
  }

  function bindDeleteSwitchedDateEventsButton() {
    $('#delete-switched-date-events-button').on('click', function() {
      var range = PeriodUtils.oneDaySpan(timesBus.getSwitchedDate());
      AjaxUtils.deleteEventsInRange(range)
               .done( onDeleteSwitchedDateEventsSuccess )
               .always(function() { });
    });
  }

  function bindModalHandlers() {
    var $modal = $('#myModal');

    $('#save-period').on('click', function() {
      modalControl.onSave();
    });

    $modal.on('hidden.bs.modal', function() {
      if (modalControl.isInSaveDraggedPeriodsState()) {
        eventsBus.clearDraggedPeriods();
      }
      else if (modalControl.isInSaveContinuePeriodState()) {
        eventsBus.clearContinuePeriod();
      }

      eventsBus.notifyOnModalClose();
    });

    $modal.on('#save-period').on('keypress', function(event) {
      if (event.ctrlKey && event.keyCode === Keys.ENTER) {
        modalControl.onSave();
      }
    });

    $modal.on('shown.bs.modal', function () {
      modalControl.focusOnTextArea();
    });
  }

  function onDeleteSwitchedDateEventsSuccess(newEvents) {
    reloadEvents(newEvents);
  }

  function bindPreviousNextDateSwitchOnLeftRightKey() {
    $(document).on('keydown', function(event) {
      var date = timesBus.getSwitchedDate();
      if (event.keyCode === Keys.LEFT) {
        timesBus.switchDate(moment(date).subtract(1, 'days'));
      }
      else if (event.keyCode === Keys.RIGHT) {
        timesBus.switchDate(moment(date).add(1, 'days'));
      }
      else if (event.keyCode === Keys.TOP) {
        timesBus.switchDate(moment(date).subtract(7, 'days'));
      }
      else if (event.keyCode === Keys.DOWN) {
        timesBus.switchDate(moment(date).add(7, 'days'));
      }
    });
  }

  function bindColorChangeOnLeftRightButtons() {

    var $leftSwitch = $('#button-switch-theme-left'),
        $rightSwitch = $('#button-switch-theme-right');

    $leftSwitch.on('click', function() {
      palette.previousColorScheme();
      switchColors(palette.getColors());
    });

    $rightSwitch.on('click', function() {
      palette.nextColorScheme();
      switchColors(palette.getColors());
    });
  }

  function switchColors(newColors) {
    _.each([ribbon, calendar, analogClock, statsBars, colorpickers, eventList, goalList, chart, modalControl],
      function(item) {
        item.setColors(newColors);
      });
  }

  function reloadEvents(newEvents) {
    eventsBus.clearEvents();
    that.onFetchEventsSuccess(newEvents);
  }
};
