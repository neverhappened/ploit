var Tags = function() {

  var allTags = ['Work', 'Fun', 'Learning', 'Design', 'Programming'],
      attachedTags = [];

  this.getAllTags = function() {
    return allTags;
  };

  this.getAttachedTags = function() {
    return attachedTags;
  };

  this.attachTag = function(tag) {
    attachedTags.push(tag);
  };

  this.wasAttached = function(tag) {
    return _.contains(attachedTags, tag);
  };

  this.clearAttachedTags = function() {
    attachedTags = [];
  }
};
