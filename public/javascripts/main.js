"use strict";

$(document).ready(main);

function main() {
  var now = moment(),
      storage = new Storage(),
      timesBus = new TimesBus(storage, now),
      eventsBus = new EventsBus(timesBus),
      goalsBus = new GoalsBus(),
      otherState = new OtherState(storage),
      palette = new ColorPalette(storage),
      digitalClock = new DigitalClock(timesBus, eventsBus),
      analogClock = new AnalogClock(timesBus, eventsBus, palette, otherState),
      ribbon = new Ribbon(timesBus, eventsBus, palette),
      calendar = new Calendar(timesBus, eventsBus, storage, palette),
      totalTime = new StatsBars(timesBus, eventsBus, palette),
      eventList = new EventList(timesBus, eventsBus, palette),
      goalList = new GoalList(timesBus, eventsBus, goalsBus, palette),
      chart = new MyChart(timesBus, eventsBus, palette),
      modalControl = new ModalControl(eventsBus, palette),
      colorPickers = new ColorPickers(ribbon, calendar, analogClock, totalTime, eventList, goalList, chart, modalControl, palette),
      guiBinder = new GUIBinder(timesBus, eventsBus, storage, palette, ribbon, calendar, colorPickers,
        analogClock, totalTime, modalControl, eventList, goalList, chart, otherState);

  AjaxUtils.fetchEvents(guiBinder.onFetchEventsSuccess);
  guiBinder.bind();

  onEverySecond(function() {
    var currentDate = moment();
    timesBus.changeNowDate(currentDate);
  });
}
