"use strict";
$(document).ready(initAll);

function initAll() {
  var storage = new Storage(),
      palette = new ColorPalette(storage);

  changeLoginButtonColor(palette.getColors());
}

function changeLoginButtonColor(colors) {
  var loginButton =  $('#loginbutton');
  loginButton.css({
    'background-color': colors.future.darknormal(),
    'border-color': colors.future.normal()
  });

  loginButton.hover(function() {
    loginButton.css({
      'background-color': colors.future.dark(),
      'border-color': colors.future.darknormal()
    });
  }, function() {
    loginButton.css({
      'background-color': colors.future.darknormal(),
    'border-color': colors.future.normal()
    });
  });
}
