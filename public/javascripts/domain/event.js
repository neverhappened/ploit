"use strict";

var Event = function(period, description, tags) {
  this.period = period;
  this.description = description;
  this.tags = tags;
};

Event.prototype.getPeriod = function() {
  return this.period.clone();
};

Event.prototype.getDescription = function() {
  return this.description;
};

Event.prototype.getTags = function() {
  return this.tags;
};

Event.prototype.setPeriod = function(newPeriod) {
  this.period = newPeriod;
};

Event.prototype.order = function() {
  var res = this.clone();
  res.period = res.period.order();
  return res;
};

Event.prototype.clone = function() {
  return (new Event(this.getPeriod(), this.description, this.tags));
};

Event.prototype.isAfter = function(other) {
  return this.period.isAfter(other.period);
};

Event.prototype.isSwitchedDay = function(switchedDate) {
  return this.period.isSwitchedDay(switchedDate);
};

Event.prototype.format = function(isPlayPeriod) {
  var isShort = PeriodUtils.isTooShort(this.period),
      formattedDescription;
  if (isPlayPeriod) {
   formattedDescription = FormatUtils.formatTimePassedFromPeriod(this.period);
  }
  else if (isShort) {
   formattedDescription = FormatUtils.formatPeriodWithDescription(this);
  }
  else {
   formattedDescription = FormatUtils.formatDescription(this);
  }
  return formattedDescription;
};

Event.prototype.isShort = function() {
  return PeriodUtils.isTooShort(this.period);
};
