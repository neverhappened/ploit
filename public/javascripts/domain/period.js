"use strict";

var Period = function(from, to) {
  this.from = from;
  this.to = to;
};

Period.prototype.getFrom = function() {
  return moment(this.from);
};

Period.prototype.getTo = function() {
  return moment(this.to);
};

Period.prototype.getDescription = function() {
  return '[no description]';
};

Period.prototype.order = function() {
  var res = this.clone();
  
  if (this.from.isAfter(this.to)) {
    var tmp = res.from;
    res.from = res.to;
    res.to = tmp;
  }

  return res;
};

Period.prototype.clone = function() {
  return new Period(moment(this.from), moment(this.to));
};

Period.prototype.isAfter = function(other) {
  return this.from.isAfter(other.from, 'minute');
};

Period.prototype.isSwitchedDay = function(switchedDate) {
  return this.from.isSame(switchedDate, 'day') && this.to.isSame(switchedDate, 'day');
};

Period.prototype.format = function(isPlayPeriod) {
  var isShort = PeriodUtils.isTooShort(this),
      formattedDescription;
  if (isPlayPeriod) {
   formattedDescription = FormatUtils.formatTimePassedFromPeriod(this);
  }
  else if (isShort) {
   formattedDescription = FormatUtils.formatPeriodWithDescription(this);
  }
  else {
   formattedDescription = FormatUtils.formatDescription(this);
  }
  return formattedDescription;
};

Period.prototype.isShort = function() {
  return PeriodUtils.isTooShort(this);
};

Period.prototype.getPeriod = function() {
  return this;
};
