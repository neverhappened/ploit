function DigitalClock(timesBus, eventsBus) {
  "use strict";
  var that = this,
      digitalClockContainer = $('.digital-clock-container span');

  timesBus.addListener(this);
  eventsBus.addListener(this);

  this.init = function() {
    that.redrawSimple();
  };

  this.onPlayContinuePeriod = function() {
    that.redrawSimple();
  };

  this.onStopContinuePeriod = function() {
    that.redrawSimple();
  };

  this.onNewNowDate = function() {
    that.redrawSimple();
  };

  this.redrawSimple = function() {
    that.redraw(timesBus.getNowDate());
  };

  this.redraw = function(date) {
    if (eventsBus.isInPlayMode()) {
      redrawPlayTime(date);
    }
    else {
      redrawTime(date);
    }
  };

  function redrawTime(date) {
    digitalClockContainer.html(date.format("Do of MMMM  YYYY, h:mm:ss a"));
  }

  function redrawPlayTime(to) {
    var from = eventsBus.getCurrentContinuePeriod().getFrom();
    digitalClockContainer.html(FormatUtils.formatTimePassed(from, to));
  }

  that.init();
}
