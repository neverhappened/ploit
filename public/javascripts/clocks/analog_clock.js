function AnalogClock(timesBus, eventsBus, palette, otherState) {
  "use strict";
  var that = this,
      canvas = document.getElementById('clock_canvas'),
      ctx = canvas.getContext("2d"),
      seeSecondHand = true,
      seeEvents = true,
      isDrawBaseForClock = false,
      sideSize = canvas.width - 5,
      circleRadius = sideSize / 2,
      centerCircleRadius = 14,
      handLineWidth = 4,
      hourHandLineWidth = 6,
      tickMarkWidth = 1,
      secondHandLineWidth = 1,
      hided = otherState.isAnalogClockShown,
      lastDate = timesBus.getNowDate(),
      temporaryTaskPeriods = [],
      clockTranslate = (($('#clock_canvas').width() - circleRadius * 2) / 2),
      colors = palette.getColors();

  timesBus.addListener(this);
  eventsBus.addListener(this);

  this.init = function() {
    that.redraw(timesBus.getNowDate(), timesBus.getSwitchedDate());
  };

  this.onNewNowDate = function() {
    that.redraw(timesBus.getNowDate());
  };

  this.onPlayContinuePeriod = function() {
    that.redrawWithLastDate();
  };

  this.onStopContinuePeriod = function() {
    that.redrawWithLastDate();
  };

  this.onSwitchDate = function() {
    that.redrawWithLastDate();
  };

  this.onDrag = function() {
    that.redrawWithLastDate();
  };

  this.hide = function() {
    if (hided) {
      return;
    }

    hided = true;
    $('#clock_canvas').hide();
  };

  this.show = function() {
    hided = false;
    this.redrawWithLastDate();
    $('#clock_canvas').show();
  };

  this.redrawWithLastDate = function() {
    this.redraw(lastDate);
  };

  this.redraw = function(date) {
    lastDate = date;

    if (hided) {
      return;
    }

    clearAll();
    ctx.translate(clockTranslate, clockTranslate);
    drawAll(timesBus.getNowDate(), timesBus.getSwitchedDate());
    ctx.translate(-clockTranslate, -clockTranslate);
  };

  this.setColors = function(newColors) {
    $.extend(colors, newColors);
    this.redrawWithLastDate();
  };

  function drawPeriods() {
    var switchedDateEvents = eventsBus.getAllPeriods();

    _.each(switchedDateEvents.concat(temporaryTaskPeriods), function(item) {
      drawPeriod(item);
    });
  }

  function drawPeriod(period) {
    period = period.order();

    var startPoint = toRadian(findAngle(period.from) - 90),
        endPoint = toRadian(findAngle(period.to) - 90);

    ctx.fillStyle = colors.period.light();
    ctx.strokeStyle = colors.period.normal();
    ctx.beginPath();
    ctx.moveTo(circleRadius, circleRadius);
    ctx.arc(circleRadius, circleRadius, circleRadius, endPoint, startPoint, true);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
  }

  function drawAll(date, switchedDate) {
    if (isDrawBaseForClock) {
      drawBaseForClock();
    }
    if (seeEvents) {
      drawPeriods();
    }

    drawAllTicks();

    if (date.isSame(switchedDate, 'day')) {
      drawHourHand(date);
      drawMinuteHand(date.minute());
      if (seeSecondHand) {
        drawSecondHand(date.second());
      }
    }

    drawCenter();
  }

  function drawBaseForClock() {
    var loc = {
          x: circleRadius,
          y: circleRadius
        },
        style = {
          fillStyle: 'fff'
        };
    drawCircle2(loc, circleRadius, style);
  }

  function drawCenter() {
    drawCircle(circleRadius, circleRadius, centerCircleRadius, colors.future.darknormal(), false, "", 0, false);
  }

  function drawAllTicks() {
    for (var angle = 0; angle < 360; angle += 1) {
      if (angle % 90 == 0)
        draw6HourTick(toRadian(angle));
      else if (angle % 30 == 0)
        drawHourTick(toRadian(angle));
      else if (angle % 6 == 0)
        drawMinuteTick(toRadian(angle));
    }
  }

  function draw6HourTick(angle) {
    var start_x = circleRadius,
        start_y = circleRadius,
        to_x = findX(start_x, angle),
        to_y = findY(start_y, angle),
        short_to_x = cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(start_x, to_x), to_x), to_x),
        short_to_y = cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(start_y, to_y), to_y), to_y);

    drawLine(short_to_x, short_to_y, to_x, to_y, tickMarkWidth, colors.future.darkest());
  }

  function drawHourTick(angle) {
    var startX = circleRadius,
        startY = circleRadius,
        toX = findX(startX, angle),
        toY = findY(startY, angle),
        short_to_x = cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(startX, toX), toX), toX), toX),
        short_to_y = cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(startY, toY), toY), toY), toY);

    drawLine(short_to_x, short_to_y, toX, toY, tickMarkWidth, colors.future.darkest());
  }

  function drawMinuteTick(angle) {
    var startX = circleRadius,
        startY = circleRadius,
        toX = findX(startX, angle),
        toY = findY(startY, angle),
        shortToX = cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(startX, toX), toX), toX), toX), toX),
        shortToY = cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(cutInHalfCoord(startY, toY), toY), toY), toY), toY);

    drawLine(shortToX, shortToY, toX, toY, tickMarkWidth, colors.future.darkest());
  }

  function drawHourHand(date) {
    var to = findHourHandLocationOnClock(date),
        fromX = circleRadius,
        fromY = circleRadius,
        shortToX = (fromX + to.x) / 2,
        shortToY = (fromY + to.y) / 2;

    drawLine(fromX, fromY, shortToX, shortToY, hourHandLineWidth, colors.future.darknormal());
  }

  function drawMinuteHand(minute) {
    var fromX = circleRadius,
        fromY = circleRadius,
        anglePerMinute = -6,
        angle = (minute - 30) * anglePerMinute,
        toX = findX(fromX, toRadian(angle)),
        toY = findY(fromY, toRadian(angle));

    drawLine(fromX, fromY, toX, toY, handLineWidth, colors.future.darknormal());
  }

  function drawSecondHand(second) {
    var from_x = circleRadius,
        from_y = circleRadius,
        start_x = from_x,
        start_y = from_y,
        anglePerSecond = -6,
        angle = (second - 30) * anglePerSecond,
        to_x = start_x + circleRadius * Math.sin(toRadian(angle)),
        to_y = start_y + circleRadius * Math.cos(toRadian(angle));

    drawLine(from_x, from_y, to_x, to_y, secondHandLineWidth, colors.future.darknormal());
  }

  function clearAll() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

// TODO: refactor - too many args

// loc = {
//   x: x,
//   y: y,
// }

// style = {

//   fillStyle: fillStyle
//
//   gradient: {
//     color0: color0,
//     color1: color1,
//   },
//
//
//   stroke: {
//     color: color,
//     width: width
//   }
// }

  function drawCircle2(loc, radius, style) {
    var startPoint = (Math.PI / 180) * 0;
    var endPoint = (Math.PI / 180) * 360;
   
    ctx.beginPath();
    ctx.arc(loc.x, loc.y, radius, startPoint, endPoint, true);

    if (style.gradient) {
      var radialGradient = ctx.createRadialGradient(radius, radius, radius / 2, radius, radius, radius);
      radialGradient.addColorStop(0, style.gradient.color0);
      radialGradient.addColorStop(1, style.gradient.color1);
      ctx.fillStyle = radialGradient;
    }
    else {
        ctx.fillStyle = style.fillStyle;
    }

    if (style.stroke) {
      ctx.strokeStyle = style.stroke.color;
      ctx.lineWidth = style.stroke.width;
      ctx.stroke();
    }

    ctx.fill();
    ctx.closePath();
  }
  function drawCircle(centerX, centerY, radius, fillStyle, isStroke, strokeColor, strokeWidth, isGradient) {
    var startPoint = (Math.PI / 180) * 0;
    var endPoint = (Math.PI / 180) * 360;
   
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, startPoint, endPoint, true);

    if (isGradient) {
      var radialGradient = ctx.createRadialGradient(radius, radius, radius / 2, radius, radius, radius);
      //radialGradient.addColorStop(0, times.baseBgColor);
      //radialGradient.addColorStop(1, times.baseBgColor);
      ctx.fillStyle = radialGradient;
    }
    else {
        ctx.fillStyle = fillStyle;
    }

    if (isStroke) {
      ctx.strokeStyle = strokeColor;
      ctx.lineWidth = strokeWidth;
      ctx.stroke();
    }

    ctx.fill();
    ctx.closePath();
  }

  function drawLine(fromX, fromY, toX, toY, width, fillStyle) {
    ctx.lineCap = 'round';
    ctx.strokeStyle = fillStyle;
    ctx.lineWidth = width;
    ctx.beginPath();
    ctx.moveTo(fromX, fromY);
    ctx.lineTo(toX, toY);
    ctx.stroke();
    ctx.closePath();
  }

  function findX(start_x, radianAngle) {
    return start_x + circleRadius * Math.sin(radianAngle); 
  }

  function findY(start_y, radianAngle) {
    return start_y + circleRadius * Math.cos(radianAngle);
  }

  function cutInHalfCoord(from, to) {
    return (from + to) / 2;
  }

  function toRadian(degree) {
    return degree * (Math.PI / 180);
  }

  function findHourHandLocationOnClock(date) {

    var hour = date.hour() % 12,
        anglePerHour = -360 / 12,
        angle = (hour - 6) * anglePerHour,
        anglePerMinute = -0.5,
        start_x = circleRadius,
        start_y = circleRadius;
        
    angle += (anglePerMinute * date.minute());

    var res_x = findX(start_x, toRadian(angle)),
        res_y = findY(start_y, toRadian(angle));

    return {
      x: res_x,
      y: res_y
    };
  }

  function findAngle(date) {
    var hour = date.hour() % 12,
        anglePerHour = 360 / 12,
        angle = hour * anglePerHour,
        anglePerMinute = 0.5;

        angle += (anglePerMinute * date.minute());
    return angle;
  }

  that.init();
}
