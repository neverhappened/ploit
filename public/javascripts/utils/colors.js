"use strict";

function colorFromRGB(rgb) {
  return (new Color(rgb.r, rgb.g, rgb.b));
}

function Color(R, G, B) {
  var that = this;

  that.r = R;
  that.g = G;
  that.b = B;

  that.toStringWithOpacity = function(opacity) {
    return "rgba(" + that.r + ", " + that.g + ", " + that.b + ", " + opacity + ")";
  };

  that.lightest = function() {
    return that.toStringWithOpacity(0.1);
  };

  that.light = function() {
    return that.toStringWithOpacity(0.2); 
  };

  that.lightnormal = function() {
        return that.toStringWithOpacity(0.3);
  };

  that.normal = function() {
    return that.toStringWithOpacity(0.4);
  };

  that.darknormal = function() {
    return that.toStringWithOpacity(0.6);
  };

  that.dark = function() {
    return that.toStringWithOpacity(0.8);
  };

  that.darkest = function() {
    return that.toStringWithOpacity(1);
  };

  that.serialize = function() {
    return {
      r: that.r,
      g: that.g,
      b: that.b
    };
  };
}
