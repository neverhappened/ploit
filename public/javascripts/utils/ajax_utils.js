var AjaxUtils = (function() {
  "use strict";

  function savePeriod(period) {
    return $.ajax({
      'type': 'POST',
      'url': 'add',
      'dataType': 'application/json',
      'contentType' : 'application/json; charset=utf-8',
      'data': JSON.stringify(PeriodUtils.convertToUnixUTC(period))
    });
  }

  function fetchEvents(onFetchEventsSuccess) {
    return $.get('events')
     .done(onFetchEventsSuccess)
     .fail(function() { /* alert( "Fail to fetch events" ); */ })
     .always(function() { /* alert( "finished" ); */ });
  }

  function dropDb() {
    return $.ajax({
      'type': 'GET',
      'url': 'dropDB',
      'success': function() {  window.location = '/'; }
    });
  }

  function deleteEventsInRange(range) {
    return $.ajax({
      'type': 'POST',
      'url': 'deleteEventsInRange',
      'dataType': 'json',
      'contentType' : 'application/json; charset=utf-8',
      'data': JSON.stringify(PeriodUtils.convertToUnixUTC(range))
    });
  }

  function updateEvent(oldEvent, updatedEvent) {
    return $.ajax({
      'type': 'POST',
      'url': 'updateEvent',
      'dataType': 'json',
      'contentType' : 'application/json; charset=utf-8',
      'data': JSON.stringify({
        'oldEvent': PeriodUtils.convertToUnixUTC(oldEvent),
        'updatedEvent': PeriodUtils.convertToUnixUTC(updatedEvent)
      })
    });
  }

  return {
    savePeriod: savePeriod,
    fetchEvents: fetchEvents,
    dropDb: dropDb,
    deleteEventsInRange: deleteEventsInRange,
    updateEvent: updateEvent
  }
})();
