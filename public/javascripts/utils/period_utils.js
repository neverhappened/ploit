var PeriodUtils = (function() {
  "use strict";

  function convertAllFromServerFormat(serverPeriods) {
    return _.map(serverPeriods, function(item) {
      return convertFromServerFormat(item);
    });
  }

  function convertFromServerFormat(serverPeriod) {
    var from = moment.unix(serverPeriod.from),
        to = moment.unix(serverPeriod.to),
        period = new Period(from, to),
        event = new Event(period, serverPeriod.description, serverPeriod.tags);

    return event;
  }

  function filterSwitchedDayEvents(eventsToFilter, switchedDate) {
    return _.select(eventsToFilter, function(item) {
      return item.isSwitchedDay(switchedDate);
    });
  }

  function filterSwitchedDayEventsForAnalogClock(eventsToFilter, switchedDate) {
    return _.chain(eventsToFilter)
      .select(function(item) {
        return item.isSwitchedDay(switchedDate);
      })
      .map(function(item) {
        return truncatePeriodForClock(item);
      });
  }

  function truncatePeriodForClock(period) {
    return period; // TODO: filter for clock side - AM or PM
  }

  function periodToPercent(period) {
      var secondsInPeriod = Math.abs(dateToDaySeconds(period.to) - dateToDaySeconds(period.from)),
          secondsInDay = 24 * 3600;
    return secondsInPeriod / secondsInDay * 100;
  }

  function toHoursFromPeriod(period) {
    var seconds = Math.abs(dateToDaySeconds(period.getPeriod().getTo()) - dateToDaySeconds(period.getPeriod().getFrom()));
    return toHoursFromSeconds(seconds);
  }

  function toAbsoluteFromMinutes(minutes) {
    return minutes / 60;
  }

  function proportionFromPeriod(period) {
    var fullSeconds = 24 * 3600,
        fromSeconds = dateToDaySeconds(period.from),
        toSeconds = dateToDaySeconds(period.to);
    return (Math.abs(toSeconds - fromSeconds)) / fullSeconds;
  }

  function convertToUnixUTC(period) {

    var res = {};
    res.description = period.getDescription();
    res.tags = period.getTags();
    res.from = moment.utc(period.getPeriod().getFrom()).unix();
    res.to = moment.utc(period.getPeriod().getTo()).unix();

    return res;
  }

  function sortPeriods(periods) {
    return periods.sort(function (p1, p2) {
      return p1.isAfter(p2);
    });
  }

  function sortMixed(mixed) {
    return mixed.sort(function(m1, m2) {
      return m1.getPeriod().isAfter(m2.getPeriod());
    });
  }

  function totalTimeFromPeriods(periods) {
    return _.reduce(periods, function(sum, each) {
      return sum + toHoursFromPeriod(each);
    }, 0);
  }

  function totalTimeInHoursFromPeriods(periods) {
    var floatingPointPeriods = totalTimeFromPeriods(periods),
        integer = +Math.floor(floatingPointPeriods),
        fraction = floatingPointPeriods - integer,
        minutesFromFraction = +(fraction * 60).toFixed();
    return {
      hours: integer,
      minutes: minutesFromFraction
    };
  }

  function isEmpty(period) {
    return period.from.isSame(period.to, 'minute');
  }

  function isHovered(period, converter, proportions, lastMouseEvent) {
    if (!lastMouseEvent.clientX) {
      return;
    }

    var rect = {
        topY: converter.dateToPixel(period.from),
        botY: converter.dateToPixel(period.to),
        leftX: converter.toRealPixelX(proportions.baseOffset),
        rightX: converter.toRealPixelX(proportions.baseOffset + proportions.ribbonBaseWidth)
      },
      pos = converter.windowToCanvas(lastMouseEvent.clientX, lastMouseEvent.clientY);

    return isInRectangle(rect, pos);
  }

  function oneHourSpan(date) {
    var from = moment(date).startOf('hour'),
        to = moment(date).startOf('hour').add(1, 'hours');

    if (!to.isSame(from, 'day')) {
      to = moment(date).endOf('day');
    }

    return {
      from: from,
      to: to
    };
  }

  function oneDaySpan(date) {
    return {
      from: moment(date).startOf('day'),
      to: moment(date).startOf('day').add(1, 'days')
    }
  }

  function secondsFromProportion(proportion) {
    var fullSeconds = 24 * 3600;
    return fullSeconds * proportion;
  }

  function toHoursFromSeconds(seconds) {
    var secondsInHours = 3600;
    return seconds / secondsInHours;
  }

  function dateToDaySeconds(date) {
    return date.hour() * 3600 + date.minute() * 60 + date.second();
  }

  function isInRectangle(rect, pos) {
    return (rect.leftX <= pos.x && rect.rightX >= pos.x) &&
      (rect.topY <= pos.y && rect.botY >= pos.y);
  }

  function percentOfHoursOfDay(periods) {
    var hours = totalTimeFromPeriods(periods);
    return hours / 24 * 100;
  }

  function periodPresent(period) {
    return period && period.from && period.to;
  }

  function continuePeriodIsPresent(continuePeriod) {
    return continuePeriod && continuePeriod.from;
  }

  function isTooShort(period) {
    return Math.abs(period.to.unix() - period.from.unix()) / 60 < 30;
  }

  return {
    convertAllFromServerFormat: convertAllFromServerFormat,
    convertFromServerFormat: convertFromServerFormat,
    filterSwitchedDayEvents: filterSwitchedDayEvents,
    filterSwitchedDayEventsForAnalogClock: filterSwitchedDayEventsForAnalogClock,
    truncatePeriodForClock: truncatePeriodForClock,
    periodToPercent: periodToPercent,
    toHoursFromPeriod: toHoursFromPeriod,
    toAbsoluteFromMinutes: toAbsoluteFromMinutes,
    convertToUnixUTC: convertToUnixUTC,
    proportionFromPeriod: proportionFromPeriod,
    sortPeriods: sortPeriods,
    sortMixed: sortMixed,
    totalTimeFromPeriods: totalTimeFromPeriods,
    totalTimeInHoursFromPeriods: totalTimeInHoursFromPeriods,
    isEmpty: isEmpty,
    isHovered: isHovered,
    oneHourSpan: oneHourSpan,
    oneDaySpan: oneDaySpan,
    percentOfHoursOfDay: percentOfHoursOfDay,
    periodPresent: periodPresent,
    continuePeriodIsPresent: continuePeriodIsPresent,
    isTooShort: isTooShort,

    date: {
      secondsFromProportion: secondsFromProportion,
      toHoursFromSeconds: toHoursFromSeconds,
      dateToDaySeconds: dateToDaySeconds
    }
  };
})();
