var TruncateUtils = (function() {
  "use strict";

  function findIntersecting(period, otherPeriods) {
    return _.select(otherPeriods, function(item) {
      return intersects(period, item);
    });
  }

  function intersects(p1, p2) {
    return !notIntersects(p1, p2);
  }

  function notIntersects(p1, p2) {
    p1 = p1.order();
    p2 = p2.order();

    return p1.to.isSame(p2.from, 'minute') || p1.to.isBefore(p2.from, 'minute') || p1.from.isAfter(p2.to, 'minute') || p1.from.isSame(p2.to, 'minute');
  }

  function truncatePeriods(toTruncate, intersectingPeriods) {

    // no intersecting = return raw
    if (intersectingPeriods.length === 0) {
      return [toTruncate];
    }
    // can be divided on 1+ truncated periods
    else {
      return truncateComplex(toTruncate, intersectingPeriods);
    }
  }

  function truncateComplex(toTruncate, intersectingPeriods) {

    intersectingPeriods = PeriodUtils.sortPeriods(intersectingPeriods);

    var res = [],
        first = intersectingPeriods[0],
        last = intersectingPeriods[intersectingPeriods.length - 1];

    toTruncate = toTruncate.order();

    // start
    if (toTruncate.from.unix() < first.from.unix()) {
      res.push(new Period(toTruncate.from, first.from));
    }

    // middle
    for (var i = 0; i < intersectingPeriods.length - 1; i++) {
      res.push(new Period(intersectingPeriods[i].to, intersectingPeriods[i + 1].from));
    }

    // end
    if (toTruncate.to.unix() > last.to.unix()) {
      res.push(new Period(last.to, toTruncate.to));
    }

    return _.select(res, function(item) {
      return !PeriodUtils.isEmpty(item);
    });
  }

  return {
    findIntersecting: findIntersecting,
    intersects: intersects,
    notIntersects: notIntersects,
    truncatePeriods: truncatePeriods
  };
})();