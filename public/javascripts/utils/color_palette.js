function ColorPalette(storage) {
  "use strict";
  var that = this,
      currentPalette = storage.loadColorPaletteIndex(0);

  this.getPalettes = function() {
    return [
      {
        past: new Color(219, 219, 219),
        today: new Color(49, 105, 145),
        future: new Color(13, 154, 186),
        period: new Color(0, 121, 181),
        switched: new Color(50, 110, 171),
        mark: new Color(255, 255, 255),
        text: new Color(0, 0, 0)
      },
      {
        past: new Color(219, 219, 219),
        today: new Color(211, 51, 51),
        future: new Color(234, 209, 89),
        period: new Color(186, 67, 67),
        switched: new Color(211, 123, 51),
        mark: new Color(255, 255, 255),
        text: new Color(0, 0, 0)
      },
      {
        past: new Color(219, 219, 219),
        today: new Color(150, 57, 57),
        future: new Color(185, 134, 92),
        period: new Color(120, 46, 88),
        switched: new Color(150, 57, 57),
        mark: new Color(255, 255, 255),
        text: new Color(0, 0, 0)
      },
    ];
  };

  this.getColors = function() {
    var defaults = getColorSchemeSerialized(),
        rawColors = storage.loadColors(defaults);

    return deserialize(rawColors);
  };

  this.setDefaultColors = function() {
    storage.clearColors();
    storage.clearSwitchedDate();
  };

  this.saveColors = function(newColors) {
    var serialized = serialize(newColors);
    storage.saveColors(serialized);
  };

  this.getColorScheme = function() {
    return that.getPalettes()[currentPalette];
  };

  this.nextColorScheme = function() {
    if (currentPalette === that.getPalettes().length - 1) {
      currentPalette = 0;
    }
    else {
      currentPalette += 1;
    }

    var newColors = that.getColorScheme();
    that.saveColors(newColors);
    storage.saveColorPaletteIndex(currentPalette);

    return newColors;
  };

  this.previousColorScheme = function() {
    if (currentPalette === 0) {
      currentPalette = that.getPalettes().length - 1;
    }
    else {
      currentPalette -= 1;
    }

    var newColors = that.getColorScheme();
    that.saveColors(newColors);
    storage.saveColorPaletteIndex(currentPalette);

    return newColors;
  };

  function getColorSchemeSerialized() {
    return serialize(that.getColorScheme());
  }

  function deserialize(rawScheme) {
    var res = {};

    for (var prop in rawScheme) {
      if (rawScheme.hasOwnProperty(prop)) {
        res[prop] = colorFromRGB(rawScheme[prop]);
      }
    }
    return res;
  }

  function serialize(scheme) {
    var res = {};

    for (var prop in scheme) {
      if (scheme.hasOwnProperty(prop)) {
        res[prop] = (scheme[prop]).serialize();
      }
    }
    return res;
  }
}
