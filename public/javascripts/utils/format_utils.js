var FormatUtils = (function() {
  "use strict";

  function formatPeriods(periods) {
    var res = '[';

    _.each(periods, function(item) {
      res += '\t' + formatPeriod(item) + ',\n';
    });

    return res + ']';
  }

  function formatPeriod(period) {
    period = period.order();
    return period.getPeriod().getFrom().format('HH:mm') + ' - ' + period.getPeriod().getTo().format('HH:mm');
  }

  function formatContinuePeriodForTaskList(continuePeriod, nowDate) {
    if (continuePeriod.to) {
      return continuePeriod.from.format('HH:mm') + ' - '  + continuePeriod.to.format('HH:mm')
    }
    else {
      return continuePeriod.from.format('HH:mm') + ' - ' + nowDate.format('HH:mm:ss');
    }
  }

  function formatContinuePeriodForRibbon(continuePeriod) {
    return continuePeriod.from.format('HH:mm') + ' - ' + continuePeriod.to.format('HH:mm:ss') + ' ' +
      formatDescription(continuePeriod);
  }

  function formatToHours(seconds) {
    return formatToHoursStr(PeriodUtils.toHoursFromSeconds(seconds));
  }

  function formatToHoursStr(hours) {
    return $.format.number(hours, '0.00') + ' hours';
  }

  function formatPeriodWithDescription(period) {
    return '(' + formatPeriod(period) + ')' + ' ' + formatDescription(period);
  }

  function formatPeriodSpanFormTaskPeriod(event) {
    var hoursWithMinutes = PeriodUtils.totalTimeInHoursFromPeriods([event]);
    return $.format.number(hoursWithMinutes.hours, '00') + ':' + $.format.number(hoursWithMinutes.minutes, '00') + 'h';
  }

  function formatToHoursFromPeriod(period) {
    var seconds = Math.abs(PeriodUtils.date.dateToDaySeconds(period.to) - PeriodUtils.date.dateToDaySeconds(period.from));
    return formatToHours(seconds);
  }

  function formatDescription(period) {
    if (!period.getDescription() || period.getDescription() == '') {
      return '[no description]';
    }
    else {
      return period.getDescription();
    }
  }

  function formatPeriodHoursTo24(periods) {
    return $.format.number(PeriodUtils.totalTimeFromPeriods(periods), '0.00') + ' / ' +
      $.format.number(24, '0.00') + ' hours';
  }

  function formatPeriodHoursToLazyHours(periods) {
    var totalHours = PeriodUtils.totalTimeFromPeriods(periods),
        lazyHours = 24 - totalHours;

    return $.format.number(totalHours, '0.00') + ' / ' + $.format.number(lazyHours, '0.00') + ' hours';
  }

  function formatHoursToPercentOfDay(periods) {
    var percent = PeriodUtils.percentOfHoursOfDay(periods);
    return $.format.number(percent, '0.0') + '%';
  }

  function formatTimePassedFromPeriod(period) {
    return formatTimePassed(period.from, period.to);
  }

  function formatTimePassed(from, to) {
    // possible bugs! TODO: check with tests
    var totalSecondsPassed = to.unix() - from.unix(),
        secondsInMinute = 60,
        minutesInHour = 60,
        secondsInHour = secondsInMinute * minutesInHour;

    var hoursPassed = Math.floor(totalSecondsPassed / secondsInHour),
        minutesPassed = Math.floor((totalSecondsPassed - hoursPassed * secondsInHour) / secondsInMinute),
        secondsPassed = Math.floor(totalSecondsPassed - hoursPassed * secondsInHour - minutesPassed * secondsInMinute),
        formattedPlayTime;
    if (hoursPassed > 0) {
      formattedPlayTime = $.format.number(hoursPassed, '00') + ':' + $.format.number(minutesPassed, '00') + ':' + $.format.number(secondsPassed, '00');
    }
    else {
      formattedPlayTime = $.format.number(minutesPassed, '00') + ':' + $.format.number(secondsPassed, '00');
    }

    return formattedPlayTime;
  }

  return {
    formatPeriods: formatPeriods,
    formatPeriod: formatPeriod,
    formatDescription: formatDescription,
    formatContinuePeriodForTaskList: formatContinuePeriodForTaskList,
    formatContinuePeriodForRibbon: formatContinuePeriodForRibbon,
    formatToHours: formatToHours,
    formatToHoursStr: formatToHoursStr,
    formatPeriodWithDescription: formatPeriodWithDescription,
    formatPeriodSpanFormTaskPeriod: formatPeriodSpanFormTaskPeriod,
    formatToHoursFromPeriod: formatToHoursFromPeriod,
    formatPeriodHoursTo24: formatPeriodHoursTo24,
    formatPeriodHoursToLazyHours: formatPeriodHoursToLazyHours,
    formatHoursToPercentOfDay: formatHoursToPercentOfDay,
    formatTimePassed: formatTimePassed,
    formatTimePassedFromPeriod: formatTimePassedFromPeriod
  }
}());
