var ChartHelper = function() {

  var that = this;

  this.allByWeek = function(events) {
    var eventsByWeek = separateByWeek(events),
        spansByWeek = calculateSums(eventsByWeek),
        spansByWeekArray = spansToArray(spansByWeek);
    return spansByWeekArray;
  };

  this.workByWeek = function(events) {
    var eventsByWorkWeek = separateByTag(events, 'Work'),
        spansByWeek = calculateSums(eventsByWorkWeek),
        spansByWeekArray = spansToArray(spansByWeek);
    return spansByWeekArray;
  };  

  this.funByWeek = function(events) {
    var eventsByWorkWeek = separateByTag(events, 'Fun'),
        spansByWeek = calculateSums(eventsByWorkWeek),
        spansByWeekArray = spansToArray(spansByWeek);
    return spansByWeekArray;
  };

  function separateByWeek(events) {
    var res = {};

    _.each(events, function(item) {
      var period = item.getPeriod(),
          from = period.getFrom();
      if (res[from.day()]) {
        res[from.day()].push(period);
      }
      else {
        res[from.day()] = [period];
      }
    });

    return res;
  }

  function calculateSums(separatedByWeekEvents) {
    var res = {};
    for (var i = 0; i < 7; i++) {
      res[i] = getSpanLength(separatedByWeekEvents[i]);
    }
    return res;
  }

  function getSpanLength(eventsForWeekDay) {
    return _.reduce(eventsForWeekDay, function(sum, event) {
      return sum + Math.abs(event.getPeriod().getTo() - event.getPeriod().getFrom()) / 1000 / 3600;
    }, 0);
  }

  function spansToArray(spanByWeek) {
    var res = [];
    for (var i = 0; i < 7; i++) {
      res.push(spanByWeek[i].toFixed(2));
    }

    return res;
  }

  function separateByTag(events, tag) {
    var res = {};
    
    events = _.select(events, function(item) {
      return _.contains(item.tags, tag);
    });

    _.each(events, function(item) {
      var period = item.getPeriod(),
          from = period.getFrom();
      if (res[from.day()]) {
        res[from.day()].push(item);
      }
      else {
        res[from.day()] = [item];
      }
    });

    return res;
  };
}
