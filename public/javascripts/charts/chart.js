var MyChart = function(timesBus, eventsBus, colorPalette) {
  "use strict";
  var that = this,
      $container = $('.chart-container'),
      canvas = document.getElementById('chart_canvas'),
      ctx = canvas.getContext('2d'),
      chartHelper = new ChartHelper(),
      colors = colorPalette.getColors(),
      lastChart;

  var pieChartCanvas = document.getElementById('pie_chart'),
      pieChartCtx = pieChartCanvas.getContext('2d');

  Chart.defaults.global.scaleFontFamily = "'Droid Serif', 'Open Sans', sans-serif";
  Chart.defaults.global.responsive = true;
  Chart.defaults.global.animation = false;
  Chart.defaults.global.scaleFontSize = 13;
  // String - Scale label font weight style
  Chart.defaults.global.scaleFontStyle = 'normal',
  Chart.defaults.global.scaleFontColor = '#888',

  eventsBus.addListener(this);
  timesBus.addListener(this);

  this.onAddEvents = function() {
    drawAll();
  };

  this.onSwitchDate = function() {
    drawAll();
  };

  this.onModalSave = function() {
    drawAll();
  };

  this.onDrag = function() {
    drawAll();
  };

  this.onDragStop = function() {
    drawAll();
  };

  this.onModalClose = function() {
    drawAll();
  };

  this.setColors = function(newColors) {
    $.extend(colors, newColors);
    drawAll();
  };

  function drawAll() {
    var events = eventsBus.getEventsForAllTime().concat(eventsBus.getDraggedPeriods());

    if (eventsBus.isInPlayMode()) {
      events.push(eventsBus.getCurrentContinuePeriod());
    }

    drawUsualChart(events);
    drawPieChart(events);
  }

  function drawUsualChart(events) {
    var spansAllByWeekArray = chartHelper.allByWeek(events),
        spansWorkByWeekArray = chartHelper.workByWeek(events),
        spansFunByWeekArray = chartHelper.funByWeek(events),
        descriptionLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    
    var data = {
        labels: descriptionLabels,
        datasets: [
          {
            label: "Full dataset",
            fillColor: colors.past.normal(),
            strokeColor: colors.past.dark(),
            pointColor: colors.past.darkest(),
            pointStrokeColor: colors.past.darkest(),
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: spansAllByWeekArray
          },
          {
            label: "Work dataset",
            fillColor: colors.today.light(),
            strokeColor: colors.today.normal(),
            pointColor: colors.today.normal(),
            pointStrokeColor: colors.today.dark(),
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: spansWorkByWeekArray
          },
          {
            label: "Fun dataset",
            fillColor: colors.future.light(),
            strokeColor: colors.future.normal(),
            pointColor: colors.future.normal(),
            pointStrokeColor: colors.future.dark(),
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: spansFunByWeekArray
          }
        ]
      };

      
    if (lastChart) {
      lastChart.clear();
    }
    var chart = new Chart(ctx).Line(data, {});
    lastChart = chart;
  }

 function drawPieChart(events) {
    
    var data = [
      {
        value: 300,
        color: colors.past.normal(),
        highlight: colors.past.darknormal(),
        label: "Red"
      },
      {
        value: 50,
        color: colors.today.normal(),
        highlight: colors.today.darknormal(),
        label: "Green"
      },
      {
        value: 100,
        color: colors.future.normal(),
        highlight: colors.future.darknormal(),
        label: "Yellow"
      }
    ];

    //var pieChart = new Chart(pieChartCtx).Doughnut(data, {});
  }
};
