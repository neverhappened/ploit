var RibbonConverter = function(canvas) {
  "use strict";
  var that = this,
      proportions = calculateRibbonProportions();

  this.translate = function() {
    return canvas.width - proportions.ribbonBaseWidth + 1;
  };

  this.eventToDate = function(event, switchedDate) {
    var canvas_x = event.clientX,
        canvas_y = event.clientY;

    return that.realPixelToDate(canvas_x, canvas_y, switchedDate);
  };

  this.realPixelToDate = function(x, y, switchedDate) {
    var loc = that.windowToCanvas(x, y);
    return that.pixelToDate(loc.y, switchedDate);
  };

  this.isPixelBeforeStart = function(x, y) {
    return that.windowToCanvas(x, y).y < proportions.firstMarkOffset;
  };

  this.dateToPercent = function(date) {
    var secondsPassed = date.second() + date.minute() * 60 + date.hour() * 3600,
        secondsInDay = 24 * 3600;
    return secondsPassed / secondsInDay;
  };

  this.datePeriodToPixelPeriod = function(datePeriod) {
    return {
      from: that.dateToPixel(datePeriod.from),
      to: that.dateToPixel(datePeriod.to)
    };
  };

  this.dateToPixel = function(date) {
    return proportions.ribbonBaseHeight * that.dateToPercent(date) + proportions.firstMarkOffset;
  };

  this.pixelToDate = function(y, switchedDate) {
    var overallPixels = proportions.ribbonBaseHeight,
        nowPixels = y - proportions.firstMarkOffset,
        ratio = nowPixels / overallPixels,
        millisecondsInDay = 24 * 3600 * 1000,
        millisecondsPassed = Math.round(millisecondsInDay * ratio);

    if (ratio > 1) {
      return moment(switchedDate).endOf('day');
    }
    else if (ratio < 0) {
      return moment(switchedDate).startOf('day');
    }
    else {
      return moment(switchedDate).startOf('day').add(millisecondsPassed, 'milliseconds');
    }
  };

  this.oneHourPixelSpan = function(date) {
    var oneHourSpan = PeriodUtils.oneHourSpan(date);
    return {
      from: that.dateToPixel(oneHourSpan.from),
      to: that.dateToPixel(oneHourSpan.to)
    };
  };

  this.windowToCanvas = function(x, y) {
    var boundingBox = canvas.getBoundingClientRect();

    return { 
      x: x - boundingBox.left * (canvas.width  / boundingBox.width),
      y: y - boundingBox.top  * (canvas.height / boundingBox.height)
    };
  };

  this.toRealPixelX = function(x) {
    return x + that.translate();
  };

  this.proportions = function() {
    return calculateRibbonProportions();
  };

  function calculateRibbonProportions() {
    var proportions = {
      ribbonBaseWidth: 105,
      baseOffset: 0,
      firstMarkOffset: 25,
      markHeight: 1,
      todayMarkWidth: 2
    };
    proportions.ribbonBaseHeight = canvas.height - proportions.firstMarkOffset;
    proportions.spanLinesLength = 0; //proportions.ribbonBaseWidth / 4;

    return proportions;
  }
};
