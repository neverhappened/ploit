function RibbonDrag(ribbon, canvas, converter, timesBus, eventsBus) {
  "use strict";
  var that = this,
      isDragged = false,
      dragStart = {},
      lastMouseEvent = {};

  this.initMouseListener = function() {
    canvas.addEventListener('mousedown', onMouseDown, false);
    canvas.addEventListener('mouseup', onMouseUp, false);
    $(window).on('mousemove', onMouseMove);
  };

  this.isDragged = function() {
    return isDragged;
  };

  this.getTruncatedDraggedPeriodOrPeriods = function() {
    var raw = that.getDraggedPeriods(),
        switchedPeriods = eventsBus.getSwitchedDayPeriodsWithContinuePeriod();

    if (timesBus.getSwitchedDate().isAfter(timesBus.getNowDate(), 'day')) {
      return [];
    }

    // include fake "period" so that you cannot drag after current time
    if (timesBus.getSwitchedDate().isSame(timesBus.getNowDate(), 'day')) {
      switchedPeriods = switchedPeriods.concat([
        new Period(timesBus.getNowDate(), moment(timesBus.getNowDate()).endOf('day'))]);
    }

    var intersecting = TruncateUtils.findIntersecting(raw, switchedPeriods);
    return TruncateUtils.truncatePeriods(raw, intersecting);
  };

  this.getDraggedPeriods = function() {
    return (new Period(dragStart, converter.eventToDate(lastMouseEvent, timesBus.getSwitchedDate())));
  };

  function onMouseDown(event) {
    var dateClicked = converter.eventToDate(event, timesBus.getSwitchedDate());
    if (!isDragged) {
      isDragged = true;
      dragStart = dateClicked;

      if (converter.isPixelBeforeStart(event.clientX, event.clientY)) {
        dragStart = moment(timesBus.getSwitchedDate()).startOf('day');
      }

      eventsBus.onDragStart();
    }
  }

  function onMouseUp(event) {
    lastMouseEvent = event;

    if (isDragged) {
      isDragged = false;

      eventsBus.onDragStop();
    }
  }

  function onMouseMove(event) {
    lastMouseEvent = event;

    var proportions = converter.proportions();

    _.each(eventsBus.getSwitchedDayEvents(), function(item) {
      item.hovered = PeriodUtils.isHovered(item.period, converter, proportions, lastMouseEvent);
    });

    if (isDragged) {
      var dragged = that.getTruncatedDraggedPeriodOrPeriods();
      eventsBus.onDrag(dragged);
    }

    ribbon.redrawWithLastDate();
  }
}
