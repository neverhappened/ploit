var RibbonDrawer = function(ribbon, canvas, ctx, converter, timesBus, eventsBus, palette) {
  "use strict";
  var dateMarksEnabled = true,
      proportions = converter.proportions(),
      isHourSpanOnRibbonShown = false,
      colors = palette.getColors();

  this.redrawAll = function(date) {
    clearAll();
    ctx.translate(converter.translate(), 0);
    drawAll(date);
    ctx.translate(-converter.translate(), 0);
  };

  this.setColors = function(newColors) {
    $.extend(colors, newColors);
  };

  function drawAll(date) {
    drawLeftBorder();
    drawBase(date);
    drawMarks(date);
    drawEvents();

    if (!date.isSame(timesBus.getSwitchedDate(), 'day')) {
      return;
    }

    drawCurrentPosition(date);
    drawContinuePeriodIfInPlayMode(date);
  }

  function drawLeftBorder() {
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(-5, 0);
    ctx.lineTo(-5, canvas.height);
    ctx.stroke();
  }

  function drawEvents() {
    var switchedDayEvents = eventsBus.getSwitchedDayEvents(),
        draggedPeriods = eventsBus.getDraggedPeriods(),
        allPeriods = switchedDayEvents.concat(draggedPeriods);

    _.each(allPeriods, function(item) {
      drawEvent(item, eventConfig(item, false));
    });
  }

  function eventConfig(event, isPlayPeriod) {
    var ordered = event.order(),
        fromMoment = ordered.getPeriod().getFrom(),
        toMoment = ordered.getPeriod().getTo(),
        from = converter.dateToPixel(ordered.getPeriod().from),
        to = converter.dateToPixel(ordered.getPeriod().to),
        color,
        lightColor,
        fromText,
        toText;

    if (event.hovered) {
      color = colors.period.darknormal();
      lightColor = colors.period.normal();
    }
    else {
      color = colors.period.normal();
      lightColor = colors.period.lightnormal();
    }

    if (isPlayPeriod) {
      fromText = fromMoment.format('HH:mm');
      toText = toMoment.format('HH:mm:ss')
    }
    else {
      fromText = fromMoment.format('HH:mm');
      toText = toMoment.format('HH:mm')
    }

    return {
      from: from,
      to: to,
      fromText: fromText,
      toText: toText,
      color: color,
      lightColor: lightColor,
      isPlayPeriod: isPlayPeriod
    }
  }

  function drawEvent(event, conf) {
    drawEventBase(conf.from, conf.to, conf.lightColor, true);

    if (!event.isShort()) {
      drawTimeTextOnStartFinish(conf.from, conf.to, event.getPeriod(), conf.color, conf.fromText, conf.toText);
    }
    drawSpanText(((conf.from + conf.to) / 2), event.format(conf.isPlayPeriod), conf.color);
  }

  function drawDraggedPeriod(period) {
    drawEventBase(conf.from, conf.to, conf.lightColor, true);
  }

  function drawTimeTextOnStartFinish(start, finish, period, color, fromText, toText) {
    var x = proportions.baseOffset - proportions.spanLinesLength - 10,
        y = start,
        text = period.from.format("HH:mm");

    ctx.fillStyle = color;
    ctx.textAlign = 'right';
    ctx.textBaseline = 'center';
    ctx.font = 'normal 18px "Droid Serif"';
    ctx.beginPath();
    ctx.fillText(fromText, x, y);

    x = proportions.baseOffset - proportions.spanLinesLength - 10;
    y = finish;
    ctx.fillText(toText, x, y);
    ctx.closePath();
  }

  function drawBase(date) {
    if (timesBus.getSwitchedDate().isSame(date, 'day')) {
      var currentTimePoint = converter.dateToPixel(date);
      drawBaseWithColor(0, currentTimePoint, colors.past.normal());
      drawBaseWithColor(currentTimePoint, canvas.height, colors.future.normal());
    } else if (timesBus.getSwitchedDate().isBefore(date, 'day')) {
      drawBaseWithColor(0, canvas.height, colors.past.normal()  );
    } else {
      drawBaseWithColor(0, canvas.height, colors.future.normal());
    }
  }

  function drawMarks(date) {
    var span = PeriodUtils.oneDaySpan(date),
        currentDate = moment(span.from);

    while (currentDate.isBefore(span.to)) {
      drawMark(currentDate);
      currentDate = moment(currentDate).add(1, 'hours');
    }
  }

  function drawCurrentPosition(date) {
    doDrawCurrentPosition(date);
  }

  function drawContinuePeriodIfInPlayMode(date) {
    if (!eventsBus.isInPlayMode()) {
        return;
    }

    var fullContinuePeriod = eventsBus.getCurrentContinuePeriod();

    if (fullContinuePeriod.isSwitchedDay(timesBus.getSwitchedDate())) {
      drawEvent(fullContinuePeriod, eventConfig(fullContinuePeriod, true));
    }
  }

  function drawBaseWithColor(from, to, color, withStroke) {
    var x = proportions.baseOffset,
      y = from,
      width = proportions.ribbonBaseWidth,
      height = to - from;

    if (withStroke) {
      ctx.lineWidth = 1;
      ctx.strokeStyle = color;
      ctx.rect(x, y, width, height);
      ctx.stroke();
    }

    ctx.fillStyle = color;
    ctx.fillRect(x, y, width, height);
  }

  function doDrawCurrentPosition(date) {
    var span = converter.oneHourPixelSpan(date);

    if (isHourSpanOnRibbonShown) {
      drawBaseWithColor(span.from, span.to, colors.period.normal());
    }

    if (eventsBus.isInPlayMode()) {
      return;
    }

    var markYPosition = converter.dateToPixel(date);
    ctx.lineWidth = proportions.todayMarkWidth;
    ctx.fillStyle = colors.period.normal();
    ctx.strokeStyle = colors.period.normal();
    ctx.lineCap = 'butt';

    var x1 = Math.round(proportions.baseOffset);
    var y1 = Math.round(markYPosition) + 0.5;
    var x2 = Math.round(x1 + proportions.ribbonBaseWidth);

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y1);
    ctx.stroke();
    ctx.closePath();

    var x = proportions.baseOffset - proportions.spanLinesLength - 10,
        y = y1,
        text = date.format("HH:mm:ss");
    ctx.fillStyle = colors.period.darknormal();
    ctx.textAlign = 'right';
    ctx.textBaseline = 'center';
    ctx.font = 'normal 18px "Droid Serif"';
    ctx.fillText(text, x, y);
  }

  function drawEventBase(from, to, color, withStroke) {
    var left = proportions.baseOffset - proportions.spanLinesLength,
        x = proportions.baseOffset,
        top = from,
        width = proportions.ribbonBaseWidth * 1.5,
        height = to - from;
    ctx.fillStyle = color;

    if (withStroke) {
      ctx.lineWidth = 1;
      ctx.strokeStyle = color;
      ctx.rect(left, top, width, height);
      ctx.stroke();
    }

    ctx.fillRect(left, top, width, height);
  }

  function drawMark(date) {
    var markYPosition = converter.dateToPixel(date) - (proportions.markHeight / 2);

    if (dateMarksEnabled) {
      drawMarkDivider(markYPosition);
    }
    drawMarkText(markYPosition, date);
  }

  function drawMarkDivider(markYPosition) {
    ctx.fillStyle = colors.text.lightest();
    ctx.lineCap = 'butt';
    ctx.fillRect(proportions.baseOffset, markYPosition, proportions.ribbonBaseWidth, proportions.markHeight);
  }

  function drawMarkText(markYPosition, date) {
    var text = date.format("HH:mm");
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'center';
    ctx.fillStyle = colors.text.lightnormal();
    ctx.font = 'normal 18px "Droid Serif"';
    ctx.fillText(text, proportions.baseOffset + (proportions.ribbonBaseWidth / 2), markYPosition);
  }

  function drawSpanText(y, text, color) {
    var x = proportions.baseOffset - proportions.spanLinesLength - 10;
    ctx.fillStyle = color;
    ctx.textAlign = 'right';
    ctx.textBaseline = 'center';
    ctx.font = 'normal 18px "Helvetica Neue"';
    ctx.fillText(text, x, y);
  }

  function clearAll() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
};
