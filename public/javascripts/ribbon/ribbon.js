function Ribbon(timesBus, eventsBus, palette) {
  "use strict";
  var that = this,
      canvas = document.getElementById('canvas-timeribbon-main'),
      ctx = canvas.getContext("2d"),
      $container = $('#timeribbon-main'),
      converter = new RibbonConverter(canvas),
      drawer = new RibbonDrawer(that, canvas, ctx, converter, timesBus, eventsBus, palette),
      lastDateDrawn = timesBus.getNowDate();

  this.drag = new RibbonDrag(that, canvas, converter, timesBus, eventsBus);

  eventsBus.addListener(that);
  timesBus.addListener(that);

  this.init = function() {
    $('#time-popup').css({ display: 'none' });
    $(canvas).css('cursor', 'default');
    that.drag.initMouseListener();
    that.redraw(timesBus.getNowDate());
    that.scrollToLastDayDrawnIfTodayIsSameAsSwitched();
  };

  this.show = function() {
    $container.show();
    that.redrawWithLastDate();
    that.scrollToLastDayDrawnIfTodayIsSameAsSwitched();
  };

  this.hide = function() {
    $container.hide();
  };

  this.onPlayContinuePeriod = function() {
    that.redrawWithLastDate();
  };

  this.onStopContinuePeriod = function() {
    that.redrawWithLastDate();
  };

  this.onEndContinuePeriod = function() {
    that.redrawWithLastDate();
  };

  this.onNewNowDate = function() {
    that.redraw(timesBus.getNowDate());
  };

  this.onModalClose = function() {
    that.redrawWithLastDate();
  };

  this.onModalSave = function() {
    that.redrawWithLastDate();
  };

  this.onDrag = function() {
    that.redrawWithLastDate();
  };

  this.onAddEvents = function() {
    that.redrawWithLastDate();
  };

  this.onUpdateEvent = function() {
    that.redrawWithLastDate();
  };

  this.onClearEvents = function() {
    that.redrawWithLastDate();
  };

  this.onSwitchDate = function() {
    that.redrawWithLastDate();
    that.scrollToLastDayDrawnIfTodayIsSameAsSwitched();
  };

  this.redrawWithLastDate = function() {
    that.redraw(lastDateDrawn);
  };

  this.redraw = function(date) {
    lastDateDrawn = moment(date);
    drawer.redrawAll(moment(lastDateDrawn));
  };

  this.setColors = function(newColors) {
    drawer.setColors(newColors);
    that.redrawWithLastDate();
  };

  this.scrollTo = function(date) {
    scrollToCurrentTime(date);
  };

  this.scrollToLastDayDrawnIfTodayIsSameAsSwitched = function() {
    if (lastDateDrawn.isSame(timesBus.getSwitchedDate(), 'day')) {
      that.scrollTo(lastDateDrawn);
    }
  };

  function scrollToCurrentTime(date) {
    var target = (Math.round(converter.dateToPercent(date) * 100) + '%'),
        duration = 0,
        settings = {};
    $container.scrollTo(target, duration, settings);
  }

  that.init();
}
