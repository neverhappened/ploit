
var ModalControl = function(eventsBus, palette) {
  "use strict";
  var that = this,
      colors = palette.getColors(),
      SAVE_DRAGGED_PERIODS = 'save_dragged_periods',
      SAVE_CONTINUE_PERIOD = 'save_continue_period',
      tags = new Tags(),
      $modal = $('#myModal'),
      $container = $('.periods-date-text-container'),
      $tagDropdown = $('#tag_dropdown'),
      $attachedTagsContainer = $('.attached_tags_container'),
      openReason = '';

  eventsBus.addListener(this);

  this.isInSaveDraggedPeriodsState = function() {
    return openReason === SAVE_DRAGGED_PERIODS;
  };

  this.isInSaveContinuePeriodState = function() {
    return openReason === SAVE_CONTINUE_PERIOD;
  };

  this.onDragStop = function() {
    var draggedPeriods = eventsBus.getDraggedPeriods(),
        minutesMinLimit = 15;
    if (PeriodUtils.totalTimeFromPeriods(draggedPeriods) < PeriodUtils.toAbsoluteFromMinutes(minutesMinLimit)) {
      eventsBus.clearDraggedPeriods();
      alert('You need to select more than ' + minutesMinLimit);
      return;
    }
    openReason = SAVE_DRAGGED_PERIODS;

    showModal(draggedPeriods);
  };

  this.onStopContinuePeriod = function() {
    var continuePeriod = eventsBus.getCurrentContinuePeriod();
    if (continuePeriod.from.isSame(continuePeriod.to, 'minute')) {
      return;
    }

    openReason = SAVE_CONTINUE_PERIOD;
    showModal([eventsBus.getCurrentContinuePeriod()]);
  };

  this.hideModal = function() {
    $modal.modal('hide');
  };

  this.onSave = function() {
    if (that.isInSaveDraggedPeriodsState()) {
      onSaveWithPeriods(eventsBus.getDraggedPeriods());
    }
    else if (that.isInSaveContinuePeriodState()) {
      onSaveWithPeriods([eventsBus.getCurrentContinuePeriod()]);
    }
  };

  this.focusOnTextArea = function() {
    $('#modal-description-text').focus();
  };

  this.setColors = function (newColors) {
    $.extend(colors, newColors);
    setOtherColors(colors);
    bindHoverEffects(colors);
  };

  that.setColors(colors);

  function onSaveWithPeriods(newPeriods) {
    var description = $('#modal-description-text')[0].value,
        attachedTags = tags.getAttachedTags(),
        newEvents = _.map(newPeriods, 
          function(item) { return (new Event(item.getPeriod(), description, attachedTags))});
    eventsBus.saveEvents(newEvents);

    _.each(newEvents, function(item) {
      AjaxUtils.savePeriod(item); // TODO save multiple in one call!
    });

    that.hideModal();
    tags.clearAttachedTags();
  }

  function showModal(periods) {
    tags.clearAttachedTags();
    redrawTags();
    $container.children().remove();

    var sorted = _.map(periods, function(item) {
      return item.order();
    });
    sorted = PeriodUtils.sortPeriods(sorted);

    _.each(sorted, function(item) {
      var $el = $('<div class="period-date-text">');
      $el.html(FormatUtils.formatPeriod(item));
      $container.append($el);
    });

    $tagDropdown.children().remove();
    _.each(tags.getAllTags(), function(tag) {
      $tagDropdown.append(option(tag));
    });

    $tagDropdown.find('li a').click(function() {
      var tagClicked = $(this).text();
      tryToAddTag(tagClicked);
    });

    $modal.modal();
    that.focusOnTextArea();
  }

  function tryToAddTag(tagText) {
    var alreadyWasAdded = tags.wasAttached(tagText);
    if (alreadyWasAdded) {
      return;
    }
    tags.attachTag(tagText);
    redrawTags();
  }

  function redrawTags() {
    $attachedTagsContainer.children().remove();

    if (tags.getAttachedTags().length === 0) {
      return;
    }

    var $tags = createTags(tags.getAttachedTags());
    $attachedTagsContainer.append($tags);
    that.setColors(palette.getColors());
  }

  function tag(tagText) {
    var $el = $('<div class="attached-tag">');
    $el.text(tagText);
    return $el;
  }

  function option(optionText) {
    var $el = $('<li>'),
        $link = $('<a href="#">');
    $link.text(optionText);
    $el.addClass(''); // TODO
    $el.append($link);
    return $el;
  }

  function createTags(tags) {
    var $el = $('<div>');
    $el.attr('class', 'task-tag');
    _.each(tags, function(item) {
      var $innerEl = $('<div>')
      $innerEl.attr('class', 'task-tag-inner');
      $innerEl.css({
        'display': 'inline',
        'float': 'none'
      });
      $innerEl.text(item);
      $el.append($innerEl);
    });
    $el.css({
      'display': 'block',
      'width': '100%',
      'border-radius': '2px',
      'padding': '5px 10px',
      'margin-bottom': '15px'
    });
    return $el;
  }

  function setOtherColors(newColors) {
    setColor($('#my-modal-header'), colors.future.darknormal());
    setColor($('.periods-date-text-container'), colors.switched.lightest());
    setColor($('#save-period'), colors.future.darknormal());
  }

  function bindHoverEffects(newColors) {

    var $modalSaveButton = $('.modal-save-button');

    setColor($modalSaveButton, newColors.future.darknormal())
    $modalSaveButton.hover(
      function() {
        setColor($(this), newColors.future.dark())
      }, 
      function() {
        setColor($(this), newColors.future.darknormal())
      });
  }

  function setColor(elem, color) {
    elem.css({
      'background-color': color
    })
  }
};
