var StatsBars = function(timesBus, eventsBus, palette) {
  "use strict";
  var that = this,
      colors = palette.getColors(),
      $container = $('#statistics-container'),
      $periodHoursTo24 = $('#period_hours_to_24'),
      $percentBar = $('#period_hours_to_lazy_hours'),
      $percentWorkHours = $('#percent_work_hours');

  bindHover($periodHoursTo24);
  bindHover($percentBar);
  bindHover($percentWorkHours);

 // eventsBus.addListener(this);
 // timesBus.addListener(this);

  this.onModalClose = function() {
    that.changeSimple();
  };

  this.onModalSave = function() {
    that.changeSimple();
  };

  this.onDrag = function() {
    that.changeSimple();
  };

  this.onDragStop = function() {
    that.changeSimple();
  };

  this.onAddEvents = function() {
    that.changeSimple();
  };

  this.onClearEvents = function() {
    that.changeSimple();
  };

  this.onSwitchDate = function() {
    that.changeSimple();
  };

  this.onNewNowDate = function() {
    that.changeSimple();
  };

  function bindHover($el) {
    $el.hover(
      function() {
        $el.css({
          'background-color': colors.period.lightnormal()
        });
      }, function() {
        $el.css('background-color', colors.period.light());
      });
  }

  this.changeSimple = function() {
    //that.change(eventsBus.getAllPeriods());
  };

  this.change = function(newEvents) {
    $periodHoursTo24.text(FormatUtils.formatPeriodHoursTo24(newEvents));
    var width = $container.width() * (PeriodUtils.percentOfHoursOfDay(newEvents) / 100);
    $percentBar.width(width);
    $percentBar.height(35);
    $percentBar.css({
      'width': width,
      'height': 35,
      'border-right': '5px solid ' + colors.period.normal()
    });
    $percentWorkHours.text(FormatUtils.formatHoursToPercentOfDay(newEvents));
  };

  this.setColors = function(newColors) {
    $.extend(colors, newColors);
    
    $periodHoursTo24.css({
      'background-color': colors.period.light()
    });
    $percentBar.css({
      'background-color': colors.period.light(),
      'border-right': '5px solid ' + colors.period.lightnormal()
    });
    $percentWorkHours.css({
      'background-color': colors.period.light()
    });
  };

  this.setColors(colors);

  this.clearEvents = function() {
    return this.change([]);
  };
};
