var GoalsBus = function() {
  "use strict";
  var that = this,
      listeners = [],
      goals = [];
      //goals = ["Ploit", "Design master"];

  this.addListener = function (listener) {
    listeners.push(listener);
  };

  this.addGoals = function (newGoals) {
    goals = goals.concat(newGoals);
    notify('onAddGoals');
  };

  this.getGoals = function() {
    return goals;
  };

  function notify(callbackString) {
    _.each(listeners, function (listener) {
      if (listener[callbackString]) {
        listener[callbackString]();
      }
    });
  }
};