function TimesBus(storage, now) {
  "use strict";
  var listeners = [],
      switchedDate = storage.loadSwitchedDate(now),
      nowDate = now;

  this.switchDate = function(newDate) {
    switchedDate = moment(newDate);
    notify('onSwitchDate');
  };

  this.changeNowDate = function(newDate) {
    nowDate = moment(newDate);
    notify('onNewNowDate');
  };

  this.getSwitchedDate = function() {
    return moment(switchedDate);
  };

  this.getNowDate = function() {
    return moment(nowDate);
  };

  this.addListener = function(listener) {
    listeners.push(listener);
  };

  this.removeListener = function(listener) {
    listeners = _.reject(listeners, function(item) {return item === listener; });
  };

  function notify(callbackString) {
    _.each(listeners, function(listener) {
      if (listener[callbackString]) {
        listener[callbackString]();
      }
    });
  }
}
