var EventsBus = function (timesBus) {
  "use strict";
  var that = this,
      listeners = [],
      events = [],
      draggedPeriods = [],
      continuePeriod = {};

  this.addListener = function (listener) {
    listeners.push(listener);
  };

  this.removeListener = function(listener) {
    listeners = _.reject(listeners, function(item) {return item === listener; });
  };

  this.getEventsForAllTime = function() {
    return events;
  };

  this.getAllPeriods = function () {
    return that.getSwitchedDayPeriodsWithContinuePeriod().concat(that.getDraggedPeriods());
  };

  this.getDraggedPeriods = function () {
    return draggedPeriods;
  };

  this.getCurrentContinuePeriod = function () {
    return (new Period(continuePeriod.from, timesBus.getNowDate()));
  };

  this.getSwitchedDayEvents = function () {
    return PeriodUtils.filterSwitchedDayEvents(events, timesBus.getSwitchedDate());
  };

  this.getSwitchedDayPeriodsWithContinuePeriod = function () {
    var switchedDayEvents = PeriodUtils.filterSwitchedDayEvents(events, timesBus.getSwitchedDate()),
        switchedDayPeriods = _.map(switchedDayEvents, function(item) { return item.period; });
    if (that.isInPlayMode()) {
      var res = switchedDayPeriods;
      if (that.getCurrentContinuePeriod().isSwitchedDay(timesBus.getSwitchedDate())) {
        res.push(that.getCurrentContinuePeriod());
      }
      return res;
    }
    else {
      return switchedDayPeriods;
    }
  };

  this.notifyOnModalClose = function () {
    notify('onModalClose');
  };

  this.playContinuePeriod = function () {
    continuePeriod.from = timesBus.getNowDate();
    notify('onPlayContinuePeriod');
  };

  this.stopContinuePeriod = function () {
    continuePeriod.to = timesBus.getNowDate();
    notify('onStopContinuePeriod');
  };

  this.endContinuePeriod = function () {
    that.clearContinuePeriod();
    notify('onEndContinuePeriod');
  };

  this.clearAllTemporary = function () {
    if (that.isInPlayMode()) {
      that.clearContinuePeriod();
    }
    that.clearDraggedPeriods();
  };

  this.clearContinuePeriod = function () {
    continuePeriod = {};
  };

  this.clearDraggedPeriods = function () {
    draggedPeriods = [];
  };

  this.isInPlayMode = function () {
    return PeriodUtils.continuePeriodIsPresent(continuePeriod);
  };

  this.saveEvents = function (newEvents) {
    events = events.concat(newEvents);
    that.clearAllTemporary();
    notify('onModalSave');
  };

  this.onDrag = function (newDraggedPeriods) {
    draggedPeriods = newDraggedPeriods;
    notify('onDrag');
  };

  this.onDragStart = function () {
  };

  this.onDragStop = function () {
    notify('onDragStop');
  };

  this.addEvents = function (newEvents) {
    events = events.concat(newEvents);
    events = PeriodUtils.sortPeriods(events);
    notify('onAddEvents');
  };

  this.updateEvent = function(oldEvent, newEvent) {

    var isThisEvent = function(item) {
      // TODO: move this to Period and Event itself
      return item.getPeriod().getFrom().isSame(oldEvent.getPeriod().getFrom(), 'minute') && 
              item.getPeriod().getTo().isSame(oldEvent.getPeriod().getTo(), 'minute');
    };

    var otherEvents = _.reject(events, isThisEvent);

    events = otherEvents;
    events.push(newEvent);
    notify('onUpdateEvent');
  };

  this.clearEvents = function () {
    events = [];
    notify('onClearEvents');
  };

  function notify(callbackString) {
    _.each(listeners, function (listener) {
      if (listener[callbackString]) {
        listener[callbackString]();
      }
    });
  }
};
