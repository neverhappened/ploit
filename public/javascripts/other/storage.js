function Storage() {
  "use strict";
  this.saveColors = function (newSerializedColors) {
    var saved = loadObject('colors'),
      res = {};

    $.extend(res, saved, newSerializedColors);
    saveObject('colors', res);
  };

  this.loadColors = function (defaults) {

    if (!isLocalStorageAvailable) {
      return defaults;
    }

    var loadedColors = loadObject('colors');

    var res = {};
    $.extend(res, defaults, loadedColors);
    return res;
  };

  this.loadColorPaletteIndex = function (defaults) {

    if (!isLocalStorageAvailable) {
      return defaults;
    }

    var index = loadObject('paletteIndex');

    if (index === undefined || index === null || index === '') {
      return defaults;
    }

    return index;
  };

  this.saveColorPaletteIndex = function (newIndex) {
    return saveObject('paletteIndex', newIndex);
  };

  this.loadSwitchedDate = function (now) {
    if (!isLocalStorageAvailable()) {
      return moment(now);
    }

    var dateInMs = loadObject('switchedDate');
    if (!dateInMs) {
      return moment(now);
    }

    return moment.unix(dateInMs);
  };

  this.loadIsAnalogClockShown = function (defaults) {
    if (!isLocalStorageAvailable()) {
      return defaults;
    }

    var isShown = loadObject('isAnalogClockShown');
    if (isShown === undefined || isShown === null || isShown === '') {
      return defaults;
    }

    return isShown;
  };

  this.loadIsTimeribbonShown = function (defaults) {
    if (!isLocalStorageAvailable()) {
      return defaults;
    }

    var isShown = loadObject('isTimeribbonShown');
    if (isShown === undefined || isShown === null || isShown === '') {
      return defaults;
    }

    return isShown;
  };

  this.saveIsAnalogClockShown = function (isShown) {
    if (!isLocalStorageAvailable) {
      return;
    }

    saveObject('isAnalogClockShown', isShown);
  };

  this.saveIsTimeribbonShown = function (isShown) {
    if (!isLocalStorageAvailable) {
      return;
    }

    saveObject('isTimeribbonShown', isShown);
  };

  this.saveSwitchedDate = function (date) {
    if (!isLocalStorageAvailable) {
      return;
    }

    saveObject('switchedDate', date.unix());
  };

  this.clearColors = function () {
    if (!isLocalStorageAvailable) {
      return;
    }

    localStorage.removeItem('colors');
  };

  this.clearSwitchedDate = function () {
    if (!isLocalStorageAvailable) {
      return;
    }

    localStorage.removeItem('switchedDate');
  };

  function saveObject(key, obj) {
    localStorage.setItem(key, JSON.stringify(obj));
  }

  function loadObject(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  function isLocalStorageAvailable() {
    try {
      return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
      return false;
    }
  }
}
