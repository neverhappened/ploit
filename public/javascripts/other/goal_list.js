var GoalList = function (timesBus, eventsBus, goalsBus, palette) {
  "use strict";
  var that = this,
      $container = $('.goals-list-side'),
      colors = palette.getColors();

  goalsBus.addListener(this);

  this.onAddGoals = function() {
    that.redrawSimple();
  };

  this.redrawSimple = function() {
    that.redraw(goalsBus.getGoals());
  };

  this.redraw = function(goals) {
    $container.children().remove();
    createElements(goals);
  };

  this.setColors = function(newColors) {
    $.extend(colors, newColors);

    $('.goal.no-goal-label').css({
      'background-color': colors.period.normal(),
      'border-radius': '0px',
      'color': '#777',
      'padding': '0px 0px'
    });
  };

  function createElements(goals) {

    if (goals.length === 0) {
      var $noGoalsLabel = createNoGoalLabel();
      $container.append($noGoalsLabel);
    }
    else {
      var $goals = _.map(goals, function(item) {
        return createGoal(item);
      });

      _.each($goals, function($goal) {
        $container.append($goal);
      });
    }
  }

  function createGoal(goal) {
    var $goal = $('<div class="goal">');
    $goal.text(goal);
    return $goal;
  }

  function createNoGoalLabel() {
    var $noGoalLabel = $('<div class="goal no-goal-label">');

    $noGoalLabel.css({
      'background-color': colors.period.normal(),
      'border-radius': '0px',
      'color': '#777',
      'padding': '0px 0px'
    });

    $noGoalLabel.text('You have no goals');

    return $noGoalLabel;
  }

  this.redrawSimple();// Test code for now
};
