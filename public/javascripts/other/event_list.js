var EventList = function (timesBus, eventsBus, palette) {
  "use strict";
  var that = this,
      $container = $('.tasks-list-side'),
      colors = palette.getColors();

  timesBus.addListener(this);
  eventsBus.addListener(this);

  this.onNewNowDate = function () {
    if (eventsBus.isInPlayMode()) {
      that.redrawSimple();
    }
  };

  this.onDrag = function () {
    that.redrawSimple();
  };

  this.onEndContinuePeriod = function () {
    that.redrawSimple();
  };

  this.onSwitchDate = function () {
    that.redrawSimple();
  };

  this.onPlayContinuePeriod = function () {
    that.redrawSimple();
  };

  this.onStopContinuePeriod = function () {
    that.redrawSimple();
  };

  this.onAddEvents = function () {
    that.redrawSimple();
  };

  this.onModalSave = function () {
    that.redrawSimple();
  };

  this.onModalClose = function () {
    that.redrawSimple();
  };

  this.redrawSimple = function () {
    var switched = eventsBus.getSwitchedDayEvents(),
      dragged = eventsBus.getDraggedPeriods();
    _.each(switched, function (item) {
      item.kind = 'simple';
    });
    _.each(dragged, function (item) {
      item.kind = 'dragged';
    });

    this.redraw(eventsBus.getSwitchedDayEvents().concat(eventsBus.getDraggedPeriods()), eventsBus.getCurrentContinuePeriod());
  };

  this.redraw = function (eventsToDraw, continuePeriod) {
    clear();
    drawEvents(eventsToDraw);
    drawContinuePeriodIfInPlayMode(continuePeriod);
    resetColors();
  };

  this.setColors = function (newColors) {
    $.extend(colors, newColors);
    $container.find('.task-simple').css({
      'background-color': colors.future.normal()
    });
    $container.find('.task-continue-period').css({
      'background-color': colors.today.normal()
    });
    $container.find('.task-dragged-period').css({
      'background-color': colors.period.normal()
    });
    $container.find('.task-tag .task-tag-inner').css({
      'background-color': colors.today.normal()
    });

    $container.find('.task input').on('focusin',
      function() {
        $(this).css({
          'background-color': colors.period.normal()
        });
      });

    $container.find('.task input').on('focusout',
      function() {
        $(this).css({
          'background-color': ''
       });
      });

    $('.task.no-events-task').css({
      'background-color': colors.period.normal(),
      'border-radius': '0px',
      'color': '#777',
      'padding': '0px 0px'
    });
  };

  function drawEvents(eventsToDraw) {
    if (eventsToDraw.length === 0) {
      drawThereIsNoEventsThisDay();
    }
    else {
      var sortedEvents = PeriodUtils.sortMixed(eventsToDraw);
      _.each(sortedEvents, function (item) {
        if (item.kind === 'simple') {
          drawSimpleEvent(item.order());
        }
        else if (item.kind === 'dragged') {
          drawDraggedPeriod(item.order());
        }
      });
    }
  }

  function drawThereIsNoEventsThisDay() {
    var $emptyLabel = $('<div class="task no-events-task">');
    $emptyLabel.text('No events this day');

    $container.append($emptyLabel);
  }

  function drawContinuePeriodIfInPlayMode(continuePeriod) {
    if (eventsBus.isInPlayMode() && continuePeriod.from.isSame(timesBus.getSwitchedDate(), 'day')) {
      drawContinuePeriod(continuePeriod);
    }
  }

  function resetColors() {
    that.setColors(palette.getColors());
  }

  function clear() {
    $container.children().remove();
  }

  function drawSimpleEvent(event) {
    var $event = createPeriod(
        event,
        'task task-simple',
        FormatUtils.formatDescription(event),
        event.getPeriod().getFrom().format('HH:mm'),
        event.getPeriod().getTo().format('HH:mm'),
        FormatUtils.formatPeriodSpanFormTaskPeriod(event.period)),
        $tags = createTags(event),
        showTags = false;
    $event.append($tags);
    $container.append($event);

    $event.find('input').on('focusin', function() {
      if (event.getTags().length !== 0) {
          $(this).parent().find('.task-tag').show();
      }
    });

    $event.find('input').on('focusout', function() {
      if (!$event.is(':hover')) {
        $(this).parent().find('.task-tag').hide();
      }
    });

    $event.hover(
      function() {
        if (event.getTags().length !== 0) {
          $(this).find('.task-tag').show();
        }
      }, function() {
        if (!$event.find('input').is(':focus')) {
          $(this).find('.task-tag').hide();
        }
      });
  }

  function drawContinuePeriod(period) {
    var $period = createPeriod(
      period,
      'task task-continue-period',
      FormatUtils.formatDescription(period),
      period.getFrom().format('HH:mm'),
      timesBus.getNowDate().format('HH:mm:ss'),
      FormatUtils.formatPeriodSpanFormTaskPeriod(new Period(period.getFrom(), timesBus.getNowDate())));
    $container.append($period);
  }

  function drawDraggedPeriod(draggedPeriod) {
    var $period = createPeriod(
      draggedPeriod,
      'task task-dragged-period',
      FormatUtils.formatDescription(draggedPeriod),
      draggedPeriod.getFrom().format('HH:mm'),
      draggedPeriod.getTo().format('HH:mm'),
      FormatUtils.formatPeriodSpanFormTaskPeriod(draggedPeriod));
    $container.append($period);
  }

  function createPeriod(period, periodClass, description, fromTimeText, toTimeText, periodText) {
    var $el = $('<div>'),
        $elText = $('<input type="description" class="task-list-input task-period-from-to-time-description">'),
        $elFrom = $('<input type="from" class="task-period-from-to-time task-list-input">'),
        $elTo = $('<input type="to" class="task-period-from-to-time task-list-input">'),
        $elPeriod = $('<span class="task-period-duration-text task-list-input">'),
        previousFromText = fromTimeText,
        previousToText = toTimeText;
    $el.attr('class', periodClass);
    $elPeriod.text(periodText + ' ');
    $elText.attr('value', description);
    $elFrom.attr('value', fromTimeText);
    $elTo.attr('value', toTimeText);

    function onFromAltered(newFromTimeHHmmText) {
      var newMoment = constructNewMoment(newFromTimeHHmmText, period.getPeriod().getFrom()),
          newPeriod = new Period(newMoment, period.getPeriod().getTo()),
          newEvent = period.clone(),
          clonedEvent = period.clone();

      newEvent.setPeriod(newPeriod);

      if (!isNewEventValid(newEvent, period)) {
        alert('date entered is not valid');
        $elFrom.val(previousFromText);
        return;
      }

      previousFromText = newFromTimeHHmmText;

      eventsBus.updateEvent(clonedEvent, newEvent);
      AjaxUtils.updateEvent(clonedEvent, newEvent);
    }

    function onToAltered(newToTimeHHmmText) {
      var newMoment = constructNewMoment(newToTimeHHmmText, period.getPeriod().getTo()),
          newPeriod = new Period(period.getPeriod().getFrom(), newMoment),
          newEvent = period.clone(),
          clonedEvent = period.clone();

      newEvent.setPeriod(newPeriod);
      
      if (!isNewEventValid(newEvent, period)) {
        alert('Date entered is not valid');
        $elTo.val(previousToText);
        return;
      }

      previousToText = newToTimeHHmmText;
      $elPeriod.text(FormatUtils.formatPeriodSpanFormTaskPeriod(newEvent));

      eventsBus.updateEvent(clonedEvent, newEvent);
      AjaxUtils.updateEvent(clonedEvent, newEvent);
    }

    function onDescriptionAltered(newDescription) {
      var clonedEvent = period.clone();
      clonedEvent.description = newDescription;

      eventsBus.updateEvent(period, clonedEvent);
      AjaxUtils.updateEvent(period, clonedEvent);
    }

    function constructNewMoment(newMomentHHmmText, periodToConstructFrom) {
      var onlyHHMM = moment(newMomentHHmmText, 'HH:mm'),
          newFromMoment = moment([
            periodToConstructFrom.year(), 
            periodToConstructFrom.month(), 
            periodToConstructFrom.date(), 
            onlyHHMM.hour(), onlyHHMM.minute()]);
      return newFromMoment;
    }

    function isNewEventValid(newEvent, oldEvent) {
      var from = newEvent.getPeriod().getFrom(),
          to = newEvent.getPeriod().getTo();
      if (!from || !to || !from.isValid() || !to.isValid()) {
        return false;
      }

      var eventsToCheckIntersectionWith = eventsBus.getSwitchedDayEvents();
      if (eventsBus.isInPlayMode()) {
        eventsToCheckIntersectionWith.push(eventsBus.getCurrentContinuePeriod());
      }

      eventsToCheckIntersectionWith = _.reject(eventsToCheckIntersectionWith, function(item) {
        return oldEvent.getPeriod().getFrom().isSame(item.getPeriod().getFrom(), 'minute') && 
                oldEvent.getPeriod().getTo().isSame(item.getPeriod().getTo(), 'minute');
      });

      var intersectingPeriods = TruncateUtils.findIntersecting(newEvent.period, eventsToCheckIntersectionWith);

      return intersectingPeriods.length === 0;
    }

    $elFrom.on('keyup', function(event) {
      if (event.keyCode === Keys.ENTER_SIMPLE) {
        $(this).blur();
        var newFromTime = $(this).val();
        onFromAltered(newFromTime);
      }
    });

    $elTo.on('keyup', function(event) {
      if (event.keyCode === Keys.ENTER_SIMPLE) {
        $(this).blur();
        var newToTime = $(this).val();
        onToAltered(newToTime);
      }
    });

    $elText.on('keyup', function(event) {
      if (event.keyCode === Keys.ENTER_SIMPLE) {
        $elText.blur();
        onDescriptionAltered($elText.val());
      }
    });

    $el.append($elText);
    $el.append($elTo);
    $el.append($elFrom);
    $el.prepend($elPeriod);
    return $el;
  }

  function createTags(event) {
    var $el = $('<div>');
    $el.attr('class', 'task-tag');
    _.each(event.tags, function(item) {
      var $innerEl = $('<div>')
      $innerEl.attr('class', 'task-tag-inner');
      $innerEl.text(item);
      $el.append($innerEl);
    });
    return $el;
  }
};
