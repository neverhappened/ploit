var ColorPickers = function (ribbon, calendar, analogClock, statsBars, eventList, goalList, chart, modalControl, palette) {
  "use strict";
  var that = this;

  this.create = function (colors) {
    if (!colors) {
      colors = palette.getColors();
    }

    $('#picker1').colpick({
      colorScheme: 'dark',
      layout: 'rgbhex',
      color: colors.past.normal(),
      onChange: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);

        var newColors = {
          past: colorFromRGB(rgb)
        };
        switchColors(newColors);
      },
      onSubmit: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);
        $(el).colpickHide();
      }
    }).css('background-color', colors.past.normal());

    $('#picker2').colpick({
      colorScheme: 'dark',
      layout: 'rgbhex',
      color: colors.today.normal(),
      onChange: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);

        var newColors = {
          today: colorFromRGB(rgb)
        };
        switchColors(newColors);
      },
      onSubmit: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);
        $(el).colpickHide();
      }
    }).css('background-color', colors.today.normal());

    $('#picker3').colpick({
      colorScheme: 'dark',
      layout: 'rgbhex',
      color: colors.future.normal(),
      onChange: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);

        var newColors = {
          future: colorFromRGB(rgb)
        };
        switchColors(newColors);
      },
      onSubmit: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);
        $(el).colpickHide();
      }
    }).css('background-color', colors.future.normal());

    $('#picker4').colpick({
      colorScheme: 'dark',
      layout: 'rgbhex',
      color: colors.period.normal(),
      onChange: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);

        var newColors = {
          period: colorFromRGB(rgb)
        };
        switchColors(newColors);
      },
      onSubmit: function (hsb, hex, rgb, el) {
        $(el).css('background-color', '#' + hex);
        $(el).colpickHide();
      }
    }).css('background-color', colors.period.normal());
  };

  this.setColors = function (newColors) {
    that.create(newColors);
  };

  function switchColors(newColors) {
    _.each([ribbon, calendar, analogClock, statsBars, chart, eventList, goalList, modalControl],
      function (item) {
        item.setColors(newColors);
      });

    palette.saveColors(newColors);
  }
};
