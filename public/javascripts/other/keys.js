var Keys = (function(){
  return {
    ENTER: 10,
    ENTER_SIMPLE: 13,
    LEFT: 37,
    RIGHT: 39,
    TOP: 38,
    DOWN: 40
  }
}());
