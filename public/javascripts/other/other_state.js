function OtherState(storage) {
  "use strict";
  this.isAnalogClockShown = storage.loadIsAnalogClockShown(false); // false by default
  this.isTimeribbonShown = storage.loadIsTimeribbonShown(true);
}
