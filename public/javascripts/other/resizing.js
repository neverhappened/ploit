function adjustTimeRibbonOnResize(ribbon) {
  "use strict";
  var $container = $('.base-container'),
      $middlePart = $('.middle-part');

  $(window).resize(function () {
    resizeTimeRibbon($container, ribbon);
    resizeMiddlePart($container, $middlePart);
  });
  resizeTimeRibbon($container, ribbon);
  resizeMiddlePart($container, $middlePart);
}

function resizeTimeRibbon($container, ribbon) {
  var containerWidth = $container.width() / 5,
    canvasWidth = containerWidth - 20,
    containerHeight = $('body').height() - 55,
    $timeribbon_container = $('#timeribbon-main'),
    $timeribbon = $('#canvas-timeribbon-main'),
    $hideButton = $('.hide-timeribbon-button-container');

  $container.height(containerHeight);
  $timeribbon_container.width(containerWidth);
  $timeribbon_container.height(containerHeight);
  $timeribbon.attr('width', canvasWidth);
  $hideButton.css({
    'margin-top': containerHeight / 2 - $hideButton.height() / 2
  });

  ribbon.scrollToLastDayDrawnIfTodayIsSameAsSwitched();
}

function resizeMiddlePart($container, $middlePart) {
  $middlePart.width($container.width() / 3.73);
}
