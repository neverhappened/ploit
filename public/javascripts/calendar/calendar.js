function Calendar(timesBus, eventsBus, storage, palette) {
  "use strict";
  var that = this,
      nowDate = timesBus.getNowDate(),
      previousDate = timesBus.getNowDate(),
      switchedDate = timesBus.getSwitchedDate(),
      colors = palette.getColors();

  eventsBus.addListener(this);
  timesBus.addListener(this);

  this.init = function() {
    that.redrawSimple(timesBus.getNowDate(), timesBus.getSwitchedDate());
  };

  this.onNewNowDate = function() {
    that.redrawIfNewDay(timesBus.getNowDate());
  };

  this.onSwitchDate = function() {
    switchedDate = timesBus.getSwitchedDate();
    that.redrawSimple(timesBus.getNowDate(), timesBus.getSwitchedDate());
  };

  this.redrawIfNewDay = function(newDate) {
    if (!newDate.isSame(previousDate, 'day')) {
      previousDate = newDate;
      that.redrawSimple(newDate, switchedDate);
    }
  };

  this.redrawSimple = function(date, newSwitchedDate) {
    nowDate = date;
    switchedDate = newSwitchedDate;

    var helper = new CalendarHelper(moment(date), moment(newSwitchedDate)),
        currentWeekRow = $('<tr>'),
        dayWeekLabelsRow = $('<tr class="day-week-labels-row">'),
        $container = $('.calendar-table');

    removeOldCells();
    $container.append(dayWeekLabelsRow);
    drawDayWeekLabels(dayWeekLabelsRow); 
    drawEmptyCells(helper, currentWeekRow);
    drawMonthCells($container, helper, currentWeekRow);
    drawMonthLabel(helper);
    this.setColors(colors);
  };

  this.setColors = function(newColors) {
    $.extend(colors, newColors);
    if (newColors.period) {
      setSwitchedColor(newColors.period.normal());
      setTodayColor(newColors.period.darknormal());
      setLabelsColor(newColors.period.normal());
    }
    if (newColors.past) {
      setPastColor(newColors.past.normal());
    }
    if (newColors.future) { 
      setFutureColor(newColors.future.normal());
    }
    if (newColors.text) {
      setTextColor(newColors.text.light());
    }
  };

  function setTextColor(color) {
    $('.calendar-cell').css({
      color: color
    });
  }

  function removeOldCells() {
    $('.calendar-table tbody tr').remove();
  }

  function drawDayWeekLabels($labelsRaw) {
    _.each(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], function(label) {
      var $cellContainer = $('<td>');
      var $cellInner = $('<div class="label-my-week">');
      $cellInner.addClass('');
      $cellInner.text(label);
      $cellContainer.append($cellInner);
      $labelsRaw.append($cellContainer);
    });
  }

  function drawEmptyCells(helper, currentWeekRow) {
    for (var i = 0; i < helper.offset(); i++) {
      var currentDayCell = $('<td>');
      var currentDayDiv = $('<div>');
      currentDayDiv.addClass('calendar-cell label-my label-my-empty');
      currentDayDiv.text();
      currentDayCell.append(currentDayDiv);
      currentWeekRow.append(currentDayCell);
    }
  }

  function addDayCell(helper, currentDate, currentWeekRow) {
    var currentDayCell = $('<td>'),
        currentDayDiv = $('<div>'),
        originalColor = colors.switched.normal();

    if (helper.isTheDaySwitched(currentDate)) {
      currentDayDiv.addClass('calendar-cell label-my label-my-switched');
    }
    else if (helper.isTheDayToday(currentDate)) {
      currentDayDiv.addClass('calendar-cell label-my label-my-today');
    }
    else if (helper.isTheDayBeforeToday(currentDate)) {
      currentDayDiv.addClass('calendar-cell label-my label-my-past');
    }
    else {
      currentDayDiv.addClass('calendar-cell label-my label-my-future');
    }
    currentDayDiv.text(currentDate);

    currentDayDiv.on('click', function() {
      switchedDate = helper.switchedDateWithDay(currentDate);
      timesBus.switchDate(switchedDate);
      storage.saveSwitchedDate(switchedDate);
    });

    currentDayCell.addClass('calendar-td');
    currentDayCell.append(currentDayDiv);
    currentWeekRow.append(currentDayCell);
  }

  function drawMonthCells($container, helper, currentWeekRow) {
    var currentDate = helper.firstDate.date(),
        lastDate = helper.lastDate.date();

    while (currentDate <= lastDate) {

      if (helper.isNewWeek(currentDate)) {
        $container.append(currentWeekRow);
        currentWeekRow = $('<tr>');
      }

      addDayCell(helper, currentDate, currentWeekRow);

      if (helper.isLastDay(currentDate)) {
        $container.append(currentWeekRow);
      }

      currentDate += 1;
    }
  }

  function drawMonthLabel(helper) {
    $('.month-switcher-label').text(helper.getCurrentMonthName());
  }

  function setPastColor(color) {
    $('.label-my-past').css({
      'background-color': color
    });
  }

  function setTodayColor(color) {
    $('.label-my-today').css({
      'background-color': color
    });
  }

  function setFutureColor(color) {
    $('.label-my-future').css({
      'background-color': color
    });
  }

  function setSwitchedColor(color) {
    $('.label-my-switched').css({
      'background-color': color
    });
  }

  function setLabelsColor(color) {
    $('.label-my-week').css({
      'background-color': color
    });
  }

  that.init();
}
