var CalendarHelper = function(todayDate, switchedDate) {
  "use strict";
  var that = this,
      months = ['January', 'February', 'March', 'April', 
                'May', 'June', 'July', 'August', 'September', 
                'October', 'November', 'December'];
  
  this.firstDate = moment(switchedDate).startOf('month');
  this.lastDate = moment(switchedDate).endOf('month');

  this.switchedDateWithDay = function(currentDate) {
    return moment([switchedDate.year(), switchedDate.month(), currentDate]);
  };

  this.isNewWeek = function(currentDate) {
    return ((currentDate + that.offset() - 1) % 7) === 0;
  };

  this.isTheDayToday = function(currentDate) {
    return that.switchedDateWithDay(currentDate).isSame(todayDate, 'day');
  };

  this.isTheDayBeforeToday = function(currentDate) {
    var dateToCompare = that.switchedDateWithDay(currentDate);
    return dateToCompare.isBefore(todayDate);
  };

  this.isTheDaySwitched = function(currentDate) {
    return that.switchedDateWithDay(currentDate).isSame(switchedDate, 'day');
  };

  this.isLastDay = function(currentDate) {
    return currentDate === that.lastDate.date();
  };

  this.getCurrentMonthName = function() {
    return months[switchedDate.month()]; // TODO use momentjs month names
  };

  this.offset = function() {
    if (that.firstDate.day() === 0) {
      return 6;
    }
    else {
      return that.firstDate.day() - 1;
    }
  };
};
