package models

import helpers._

import scala.concurrent._ 
import play.api._

case class User(id: Long, email: String, encryptedPassword: String, privilege: String) {
  def isAdmin = 
    privilege == User.admin
  def isUnprivileged =
    privilege == User.unprivileged
}

object User {

  val unprivileged = "unprivileged"
  val admin = "admin"

  val dbHelper = new UserHelper()

  def tryToLogin(userLoginData: UserLoginData): LoginAttempt = {
    val maybeUser: Option[User] = dbHelper.getUser(userLoginData)
    maybeUser match {
      case Some(user) => {
        if (AuthHelper.isRightPasswordEntered(user, userLoginData))
          LoginSuccess(user)
        else
          LoginFailure
      }
      case None => LoginFailure
    }
  }

  def createUnprivilegedUser(registrationData: RegistrationOK): User = {
    dbHelper.addUnprivilegedUser(registrationData)
  }

  def tryToConvertRegisterDataIntoUser(userRegisterData: UserRegisterData): RegisterAttempt = {
    Logger.info((emailHolded(userRegisterData) ||
        confirmationDoesNotMatch(userRegisterData)).toString)

    if (emailHolded(userRegisterData)) {
      EmailHolded
    }
    else if (confirmationDoesNotMatch(userRegisterData)) {
      ConfirmationDoesntMatch
    }
    else {
      val encryptedPassword = AuthHelper.encrypt(userRegisterData)
      RegistrationOK(userRegisterData.email, encryptedPassword)
    }
  }

  def emailHolded(userRegisterData: UserRegisterData) = 
    dbHelper.isEmailHolded(userRegisterData)
  def confirmationDoesNotMatch(userRegisterData: UserRegisterData) = 
    (userRegisterData.rawPassword != userRegisterData.rawPasswordConfirmation)
}

case class UserLoginData(email: String, rawPassword: String)

case class UserRegisterData(email: String, rawPassword: String, rawPasswordConfirmation: String)

sealed trait LoginAttempt
case class LoginSuccess(user: User) extends LoginAttempt
case object LoginFailure extends LoginAttempt

sealed trait RegisterAttempt
case class RegistrationOK(email: String, encryptedPassword: String) extends RegisterAttempt
case object ConfirmationDoesntMatch extends RegisterAttempt
case object EmailHolded extends RegisterAttempt
