package models

import helpers._

import com.github.nscala_time.time.Imports._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api._

case class Event(val description: String, val from: Long, val to: Long, val tags: List[String])
case class DbEvent(val id: Long, val event: Event)

object Event {

  type RawPeriod = (Option[String], Option[Long], Option[Long], Option[List[String]])
  type RawRange = (Option[Long], Option[Long])

  val dbHelper = new EventHelper()

  def all(user: User): List[Event] = dbHelper.listEventsForUser(user)

  def all2Json(user: User) = {
    Json.toJson(all(user) map event2Json)
  }

  def add(maybeEvent: Option[Event], user: User) {
    maybeEvent match {
      case Some(event) =>
        dbHelper.add(event, user)
      case _ =>
        Logger.error("Fail to parse event")
    }
  }

  def update(maybeOldEvent: RawPeriod, maybeUpdatedEvent: RawPeriod, userEmail: String) {
    rawToUpdatedData(maybeOldEvent, maybeUpdatedEvent) match {
      case (Some(oldEvent), Some(updatedEvent)) =>
        dbHelper.update(oldEvent, updatedEvent)
      case _ =>
        None
    }
  }

  def add(period: RawPeriod, user: User) {
    add(period2Event(period), user)
  }

  def deleteEventsInRange(user: User, maybeRange: Option[TimeRange]) {
    maybeRange match {
      case Some(range) =>
        dbHelper.deleteEventsInRange(user, range)
      case _ =>
        Logger.error("Fail to parse timerange")
    }
  }

  def deleteEvents(user: User) {
    dbHelper.deleteEvents(user)
  }

  private 
  def period2Event(optionalPeriod: RawPeriod): Option[Event] = {
    optionalPeriod match {
      case (Some(description), Some(from), Some(to), Some(tags)) =>
        Some(Event(description, from, to, tags))
      case _ =>
        None
    }
  }

  private 
  def event2Json(event: Event) = {
    Json.toJson(Map(
      "description" -> Json.toJson(event.description),
      "from" -> Json.toJson(event.from),
      "to" -> Json.toJson(event.to),
      "tags" -> Json.toJson(event.tags)
    ))
  }

  private
  def rawToUpdatedData(maybeOldEvent: RawPeriod, maybeUpdatedEvent: RawPeriod): 
    (Option[Event], Option[Event]) = {

    val oldEvent = period2Event(maybeOldEvent)
    val updatedEvent = period2Event(maybeUpdatedEvent)

    (oldEvent, updatedEvent)
  }
}
