package models

case class TimeRange(from: Long, to: Long)

object TimeRange {
  def fromJson(maybeRawRange: (Option[Long], Option[Long])): Option[TimeRange] = maybeRawRange match {
    case (Some(from), Some(to)) =>
      Some(TimeRange(from, to))
    case _ =>
      None
  }
}
