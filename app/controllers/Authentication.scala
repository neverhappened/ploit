package controllers

import models._
import helpers._

import play.api._
import play.api.mvc._
import com.github.nscala_time.time.Imports._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.Form
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data._
import play.api.data.format.Formats._


object Authentication extends Controller with Secured {

  val emailField = "email"
  val passwordField = "rawPassword"
  val rawPasswordConfirmationField = "rawPasswordConfirmation" 

  val loginForm = Form(
    mapping(
      emailField -> email,
      passwordField -> nonEmptyText)
    (UserLoginData.apply)(UserLoginData.unapply)
  )

  val registerForm = Form(
    mapping(
      emailField -> email,
      passwordField -> nonEmptyText(minLength=8),
      rawPasswordConfirmationField-> nonEmptyText(minLength=8)
    )(UserRegisterData.apply)(UserRegisterData.unapply)
  )

  def register = Action { implicit request =>
    Logger.debug("register()")
    Ok(views.html.register(registerForm))
  }

  def signUp = Action { implicit request =>
    Logger.debug("signUp()")

    registerForm.bindFromRequest.fold(
      formWithErrors => {
        Logger.debug("authenticate() User: NONE")
        Logger.debug("errors: " + formWithErrors.errors)
        Ok(views.html.register(formWithErrors))
      },
      userRegisterData => {
        Logger.debug("authenticate() User: " + userRegisterData)
        tryToRegister(userRegisterData)
      })
  }

  def tryToRegister(userRegisterData: UserRegisterData)(implicit request: Request[AnyContent]): Result = {
    val attempt = User.tryToConvertRegisterDataIntoUser(userRegisterData)
    attempt match {
      case ok: RegistrationOK => {
        val newUser = User.createUnprivilegedUser(ok)
        authenticateAfterSignUp(newUser)
      }
      case EmailHolded => {
        val formWithErrors = registerForm.withError(emailField, "Email already holded")
        Ok(views.html.register(formWithErrors))
      }
      case ConfirmationDoesntMatch => {
        val formWithErrors = registerForm.withError(emailField, "Confirmation doesn't match")
        Ok(views.html.register(formWithErrors))
      }
      case _ => {
        throw new IllegalStateException("This case wasn't handled at all")
      }
    }
  }

  def login = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        Logger.debug("authenticate() User: NONE")
        Ok(views.html.login(formWithErrors))
      },
      userLoginData => {
        Logger.debug("authenticate() User: " + userLoginData)
        tryToAuthenticate(userLoginData)(request)
      })
  }

  def logout = withAuth { username => implicit request =>
    Redirect(routes.Application.index()).withSession(
      request.session - Security.username
    )
  }

  def tryToAuthenticate(loginData: UserLoginData)(implicit request: Request[AnyContent]): Result = {
    User.tryToLogin(loginData) match {
      case LoginSuccess(user) => {
        Logger.info("Login success: " + user)
        Redirect(routes.Application.index()).withSession(
          request.session + (Security.username -> user.email)
        )
      }
      case LoginFailure => {
        Logger.debug("Login failure. " + loginData)
        Logger.debug("Available users: \n" + userHelper.listUsers.mkString("\n"))
        val formWithErrors = loginForm.withError(FormError(emailField, "Wrong email or password"));
        Ok(views.html.login(formWithErrors))
      }
    }
  }

  def authenticateAfterSignUp(user: User)(implicit request: Request[AnyContent]): Result = {
    Redirect(routes.Application.index()).withSession(
      request.session + (Security.username -> user.email)
    )
  }
}
