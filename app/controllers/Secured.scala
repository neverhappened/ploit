package controllers

import helpers._
import models._

import play.api._
import play.api.mvc._


trait Secured {

  var userHelper = new UserHelper()

  def username(request: RequestHeader) = request.session.get(Security.username)

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Authentication.login)

  def withAuth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(username, onUnauthorized) { user =>
      Action(request => f(user)(request))
    }
  }

  def withUser(f: User => Request[AnyContent] => Result) = withAuth { email => implicit request =>
    userHelper.getUserByEmail(email).map { user =>
      f(user)(request)
    }.getOrElse(onUnauthorized(request))
  }

  def withAdmin(f: User => Request[AnyContent] => Result) = withUser { user => implicit request =>
    if (user.isAdmin) {
      f(user)(request)
    }
    else {
      onUnauthorized(request)
    }
  }
}
