package controllers

import models._
import helpers._

import play.api._
import play.api.mvc._
import com.github.nscala_time.time.Imports._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.Form
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data._
import play.api.data.format.Formats._


object Application extends Controller with Secured {

  def index = withUser { user => implicit request =>
    Ok(views.html.index(user))
  }

  def events = withUser { user => implicit request =>
    Logger.info("events()")
    
    val json = Event.all2Json(user)
    Ok(json)
  }

  def add = withUser { user => implicit request =>
    val maybeMaybeEvent = request.body.asJson.map { json =>
      val maybeEvent = parsePeriodFromBody(json)
      Logger.info("add():  " + maybeEvent)
      Event.add(maybeEvent, user)
    }

    Ok("")
  }

  def update = withUser { user => implicit request =>
    val res = request.body.asJson.map { json =>
      val maybeOldEvent = parsePeriodFromBody(json \ "oldEvent")
      val maybeUpdatedEvent = parsePeriodFromBody(json \ "updatedEvent")
      Logger.info("update():  " + "oldEvent: " + maybeOldEvent + " updatedEvent: " + maybeUpdatedEvent);
      Event.update(maybeOldEvent, maybeUpdatedEvent, user.email)
    }

    Ok("")
  }

  def dropUsers = withAdmin { user => implicit request =>
    SchemaLoader.dropUsers
    Redirect(routes.Authentication.register())
  }

  def dropDB = withAdmin { user => implicit request =>
    SchemaLoader.dropDB
    Redirect(routes.Authentication.register())
  }

  // dont route to this method for now - do it when you'll have special page with settings, because its dangerous option
  def deleteEventsForUser = withUser { user => implicit request =>
    Event.deleteEvents(user)
    Redirect(routes.Application.index())
  }

  def deleteEventsInRange = withUser { user => implicit request =>
    val maybeMaybeRange = request.body.asJson.map { json =>
      val maybeRange = parseRangeFromBody(json)
      Logger.info("delete(): " + maybeRange)
      Event.deleteEventsInRange(user, TimeRange.fromJson(maybeRange))
    }
    
    val json = Event.all2Json(user)
    Ok(json)
  }

  def dropEvents = withAdmin { user => implicit request =>
    SchemaLoader.dropEvents
    Redirect(routes.Application.index())
  }

  private
  def parsePeriodFromBody(body: JsValue): (Option[String], Option[Long], Option[Long], Option[List[String]]) = {
    ((body \ "description").asOpt[String],
     (body \ "from").asOpt[Long],
     (body \ "to").asOpt[Long],
     (body \ "tags").as[JsArray].asOpt[List[String]])
  }

  private
  def parseRangeFromBody(body: JsValue): (Option[Long], Option[Long]) = {
    ((body \ "from").asOpt[Long],
     (body \ "to").asOpt[Long])
  }
}
