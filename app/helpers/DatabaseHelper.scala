package helpers

import models._

import java.net.URI
import java.sql._

object PloitConnection {
  def getConnection(): Connection = {
    if (System.getenv("DATABASE_URL") == null)
      getDevConnection
    else
      getProdConnection
  }

  // TODO: use variables
  // todo use connection pool: https://www.playframework.com/documentation/2.3.x/Home
  def getProdConnection(): Connection = {
    val dbUri = new URI(System.getenv("DATABASE_URL")) 
    val username = dbUri.getUserInfo.split(":")(0)
    val password = dbUri.getUserInfo.split(":")(1)
    val dbUrl = "jdbc:postgresql://" + dbUri.getHost + dbUri.getPath
    DriverManager.getConnection(dbUrl, username, password)
  }

  def getDevConnection(): Connection = {
    val username = "test_user"
    val password = "test"
    val hostname = "localhost"
    val dbUrl = "jdbc:postgresql://" + hostname + ":5432/test_db"
    DriverManager.getConnection(dbUrl, username, password) 
  }
}

object SchemaLoader {
  def ensureSchemaLoaded(connection: Connection) {
    createUsersTable(connection)
    createEventsTable(connection)
    createTagsTable(connection)
  }

  def createUsersTable(connection: Connection) {
    val stmt = connection.createStatement
    stmt.executeUpdate("CREATE TABLE IF NOT EXISTS users " + 
      "(id SERIAL PRIMARY KEY, " + 
      "tick timestamp NOT NULL, " +
      "email TEXT NOT NULL UNIQUE, " +
      "encryptedPassword TEXT NOT NULL, " +
      "privilege TEXT NOT NULL)")
  }

  def createEventsTable(connection: Connection) {
    val stmt = connection.createStatement
    stmt.executeUpdate("CREATE TABLE IF NOT EXISTS events " + 
      "(id SERIAL PRIMARY KEY, " + 
      "user_id BIGINT NOT NULL references users(id), " +
      "tick timestamp NOT NULL, " +
      "description TEXT NOT NULL, " +
      "fromtime BIGINT NOT NULL, " +
      "totime BIGINT NOT NULL)")
  }

  def createTagsTable(connection: Connection) {
    val stmt = connection.createStatement
    stmt.executeUpdate("CREATE TABLE IF NOT EXISTS event_tags " + 
      "(id SERIAL PRIMARY KEY, " + 
      "event_id BIGINT NOT NULL references events(id), " +
      "tag_str varchar(200))")
  }

  def dropDB() {
    val connection = PloitConnection.getConnection
    val stmt = connection.createStatement
    stmt.executeUpdate("DROP TABLE event_tags")
    stmt.executeUpdate("DROP TABLE events")
    stmt.executeUpdate("DROP TABLE users")
  }

  def dropEvents() {
    val connection = PloitConnection.getConnection
    val stmt = connection.createStatement
    stmt.executeUpdate("DROP TABLE events")
  }

  def dropUsers() {
    val connection = PloitConnection.getConnection
    val stmt = connection.createStatement
    stmt.executeUpdate("DROP TABLE users")
  }
}

object IDUtils {
  def getGeneratedKey(stmt: PreparedStatement): Long = {
    try {
      val generatedKeys: ResultSet = stmt.getGeneratedKeys()
      if (generatedKeys.next()) {
        return generatedKeys.getLong(1)
      }
      else {
        throw new SQLException("Creating user failed, no ID obtained.");
      }
    }
    catch {
      case e: Exception => throw new IllegalStateException(e.getMessage())
    }
  }
}

class EventHelper {

  val connection = PloitConnection.getConnection()

  def add(event: Event, user: User) {
    SchemaLoader.ensureSchemaLoaded(connection)

    val sql = "INSERT INTO events(tick, user_id, description, fromtime, totime) VALUES (now(),?,?,?,?)"
    val stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
    stmt.setLong(1, user.id)
    stmt.setString(2, event.description)
    stmt.setLong(3, event.from)
    stmt.setLong(4, event.to)

    val affectedRows = stmt.executeUpdate()
    if (affectedRows == 0) {
      throw new java.sql.SQLException("Creating user failed, no rows affected.");
    }

    val id = IDUtils.getGeneratedKey(stmt)
    val dbEvent = DbEvent(id, event)
    addTags(dbEvent)
  }

  def update(oldEvent: Event, updatedEvent: Event) {
    SchemaLoader.ensureSchemaLoaded(connection)

    val sql = "UPDATE events SET fromtime=?, totime=?, description=? WHERE fromtime=? AND totime=?"
    val stmt = connection.prepareStatement(sql)
    stmt.setLong(1, updatedEvent.from)
    stmt.setLong(2, updatedEvent.to)
    stmt.setString(3, updatedEvent.description)
    stmt.setLong(4, oldEvent.from)
    stmt.setLong(5, oldEvent.to)
    stmt.execute()
  }

  def addTags(dbEvent: DbEvent) {
    for (tag <- dbEvent.event.tags)
      addTag(dbEvent, tag)
  }

  def addTag(dbEvent: DbEvent, tag: String) {
    val sql = "INSERT INTO event_tags(event_id, tag_str) VALUES (?, ?)"
    val stmt = connection.prepareStatement(sql)
    stmt.setLong(1, dbEvent.id)
    stmt.setString(2, tag)
    stmt.execute()
  }

  def listEvents: List[Event] = {
    SchemaLoader.ensureSchemaLoaded(connection)

    val query = "SELECT id, description, fromtime, totime FROM events"
    val stmt = connection.createStatement

    val rs = stmt.executeQuery(query)

    var res: List[Event] = List[Event]()

    while (rs.next()) {
      val id = rs.getLong("id")
      val description = rs.getString("description")
      val from = rs.getLong("fromtime")
      val to = rs.getLong("totime")
      val tags = listTags(id)
      
      res ::= Event(description, from, to, tags)
    }

    res
  }

  def listEventsForUser(user: User): List[Event] = {
    SchemaLoader.ensureSchemaLoaded(connection)

    val query = "SELECT id, description, fromtime, totime FROM events WHERE user_id=?"
    val stmt = connection.prepareStatement(query)
    stmt.setLong(1, user.id)

    val rs = stmt.executeQuery()
    var res: List[Event] = List[Event]()

    while (rs.next()) {
      val id = rs.getLong("id")
      val description = rs.getString("description")
      val from = rs.getLong("fromtime")
      val to = rs.getLong("totime")
      val tags = listTags(id)
      
      res ::= Event(description, from, to, tags)
    }

    res
  }

  def listTags(eventId: Long): List[String] = {
    val query = "SELECT tag_str FROM event_tags WHERE event_id=?"
    val stmt = connection.prepareStatement(query)
    stmt.setLong(1, eventId)

    val rs = stmt.executeQuery()
    var res = List[String]()

    while (rs.next()) {
      val tag = rs.getString("tag_str")
      res ::= tag
    }

    res
  }

  def deleteEvents(user: User) {
    SchemaLoader.ensureSchemaLoaded(connection)

    val query = "DELETE FROM events WHERE user_id=?"
    val stmt = connection.prepareStatement(query)
    stmt.setLong(1, user.id)

    stmt.execute()
  }

  def deleteEventsInRange(user: User, range: TimeRange) {
    SchemaLoader.ensureSchemaLoaded(connection)

    deleteTagsInRange(range)
    doDeleteEventsInRange(user, range)
  }

  def deleteTagsInRange(range: TimeRange) {
    val query = "DELETE FROM event_tags WHERE fromtime >= ? AND totime < ?"
    val stmt = connection.prepareStatement(query)
    stmt.setLong(1, range.from)
    stmt.setLong(2, range.to)

    stmt.execute()
  }
  def doDeleteEventsInRange(user: User, range: TimeRange) {
    val query = "DELETE FROM events WHERE user_id=? AND fromtime >= ? AND totime < ?"
    val stmt = connection.prepareStatement(query)
    stmt.setLong(1, user.id)
    stmt.setLong(2, range.from)
    stmt.setLong(3, range.to)

    stmt.execute()
  }
}

class UserHelper {
  val connection = PloitConnection.getConnection

  def addUnprivilegedUser(registrationData: RegistrationOK): User = {
    SchemaLoader.ensureSchemaLoaded(connection)

    val sql = "INSERT INTO users(tick, email, encryptedPassword, privilege) VALUES (now(),?,?,?)"
    val stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
    stmt.setString(1, registrationData.email)
    stmt.setString(2, registrationData.encryptedPassword)
    stmt.setString(3, User.unprivileged)
    
    val affectedRows = stmt.executeUpdate()
    if (affectedRows == 0) {
      throw new java.sql.SQLException("Creating user failed, no rows affected.");
    }

    val id = IDUtils.getGeneratedKey(stmt)
    User(id, registrationData.email, registrationData.encryptedPassword, User.unprivileged)
  }

  def listUsers: List[User] = {
    SchemaLoader.ensureSchemaLoaded(connection)

    val query = "SELECT id, email, encryptedPassword, privilege FROM users"
    val stmt = connection.createStatement

    val rs = stmt.executeQuery(query)

    var res: List[User] = List[User]()

    while (rs.next())
      res ::= parseUser(rs)

    res
  }

  def getUser(userLoginData: UserLoginData): Option[User] = {
    SchemaLoader.ensureSchemaLoaded(connection)

    getUserByEmail(userLoginData.email)
  }

  def getUserByEmail(email: String): Option[User] = {
    SchemaLoader.ensureSchemaLoaded(connection)

    val sql = "SELECT id, email, encryptedPassword, privilege FROM users WHERE email=?"
    val stmt = connection.prepareStatement(sql)
    stmt.setString(1, email)
    val rs = stmt.executeQuery()
    if (rs.next())
      Some(parseUser(rs))
    else
      None
  }

  def isEmailHolded(userRegisterData: UserRegisterData): Boolean = {
    SchemaLoader.ensureSchemaLoaded(connection)
    val query = "SELECT * FROM users WHERE email=?"
    val stmt = connection.prepareStatement(query)
    stmt.setString(1, userRegisterData.email)

    val rs = stmt.executeQuery()
    return rs.next()
  }

  private
  def parseUser(rs: ResultSet): User = {
    val id = rs.getLong("id")
    val email = rs.getString("email")
    val encryptedPassword = rs.getString("encryptedPassword")
    val privilege = rs.getString("privilege")
    
    User(id, email, encryptedPassword, privilege)
  }
}
