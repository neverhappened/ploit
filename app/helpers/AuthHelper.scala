package helpers

import models._
import com.github.t3hnar.bcrypt._
import play.api._

object AuthHelper {

  def encrypt(userRegisterData: UserRegisterData) = {
    val salt = generateSalt
    val encryptedPassword = userRegisterData.rawPassword.bcrypt(salt)

    encryptedPassword
  }

  def isRightPasswordEntered(user: User, userLoginData: UserLoginData): Boolean = {
    val enteredEncryptedPassword = userLoginData.rawPassword.bcrypt(user.encryptedPassword)
    val etalonEncryptedPassword = user.encryptedPassword

    Logger.info("Compare \n\nentered: " + enteredEncryptedPassword + "\netalon:  " + etalonEncryptedPassword + "\n\n")
    userLoginData.rawPassword.isBcrypted(etalonEncryptedPassword)
  }
}

